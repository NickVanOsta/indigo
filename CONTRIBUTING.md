# Contribution Guidelines

Here you can find the guidelines to be followed when developing new features/bugfixes for this project.

[[_TOC_]]

## Go version

This project is written with Go v1.16.3.
Other versions of Go might not give the same results.

### Project setup

This project uses a lot of bash scripts, so it is recommended to develop in a Unix environment.

[Git LFS](https://git-lfs.github.com/) is used for large binary files such as images and videos.
Make sure it is installed, then use the following command to initialise it:

```bash
git lfs install
```

You only have to do this once per repository per user.

Just run the following command to initialize the repository and set up the git hooks.

```bash
make init
```

Now everything is in place to start programming.

### List all available make commands with a small description

```bash
make help
```

### Initialize the project

```bash
make init
```

### Build the project

```bash
make build
# or
make
```

### Run the application

```bash
make run
```

### Lint the files

```bash
make lint
```

### Run unit tests

```bash
make test
```

### Run data race detector

```bash
make race
```

### Run memory sanitizer

```bash
make msan
```

### Run benchmark tests

```bash
make benchmark
```

### Generate global code coverage report

```bash
make coverage
```

### Generate global code coverage report in HTML

```bash
make coverhtml
```

### Remove previous build results

```bash
make clean
```

## Git Guidelines

### The Flow

The flow used in this repository is based off of [GitFlow](https://nvie.com/posts/a-successful-git-branching-model/), created by Vincent Driessen.

### Branches

- **prod:** The production branch containing production ready code.
- **dev (default):** The development branch containing features and fixes that are ready for the next release.
- **feature/...:** Branches containing changes linked to a certain new feature.
- **bugfix/...:** Branches containing changes linked to a certain bug report.
- **chore/...:** Branches containing changes linked to a certain task that is neither a feature nor a bugfix.
- **release/...:** Branches containing changes to prepare for a new production release.
- **hotfix/...:** Branches containing changes linked to a certain bug report, these handle severe bugs on the production branch.

`prod` and `dev` are not pushable by **anyone**.

### Commit Rules

Commit messages are linted and checked upon commiting your changes.
The linting is based off of [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).

**Form:** type\[(scope)\]\[!\]: ...

- **type:** This defines the type of changes made in the commit. Valid options are:
  - `feat`: You added a new feature.
  - `fix`: You fixed something.
  - `test`: You added/updated tests.
  - `refactor`: You did some refactoring.
  - `perf`: You made changes to improve performance.
  - `style`: You made style changes.
  - `docs`: You added/updated documentation.
  - `build`: You made changes that affect the build system.
  - `ci`: You made changes to the CI configuration.
  - `chore`: You did some other chore.
- **scope (OPTIONAL):** This defines the commit's scope.
- **! (OPTIONAL):** This indicates that the commit contains breaking changes.

#### Examples

feat: this is my commit

fix!: this is my breaking commit

chore(scope): this is my scoped commit

perf(scope)!: this is my scoped breaking commit

### Steps for implementing a new feature

1. Make sure your local `dev` branch is up to date.
2. Create a new branch from `dev`, name it `feature/#n-...` where `n` is the issue ID and end it with a short description of the feature.
3. Make your changes.
4. Make sure your local `dev` branch is up to date and rebase `dev` into your branch.
5. When you're ready create a merge request for your branch to merge into `dev`.
6. Add the new feature to the [changelog](CHANGELOG.md) and reference the created merge request.
7. Discuss and fix any issues the reviewers might see in your code.
8. Wait for the feature to be merged.

Don't create your `feature` branches from any other branch than `dev` to prevent git :spaghetti:!

### Steps for implementing a new bugfix

1. Make sure your local `dev` branch is up to date.
2. Create a new branch from `dev`, name it `bugfix/#n-...` where `n` is the issue ID and end it with a short description of the bug to be fixed.
3. Make your changes.
4. Make sure your local `dev` branch is up to date and rebase `dev` into your branch.
5. When you're ready create a merge request for your branch to merge into `dev`.
6. Add the new bugfix to the [changelog](CHANGELOG.md) and reference the created merge request.
7. Discuss and fix any issues the reviewers might see in your code.
8. Wait for the bugfix to be merged.

Don't create your `bugfix` branches from any other branch than `dev` to prevent git :spaghetti:!

### Steps for implementing a new chore

1. Make sure your local `dev` branch is up to date.
2. Create a new branch from `dev`, name it `chore/#n-...` where `n` is the issue ID and end it with a short description of the chore.
3. Make your changes.
4. Make sure your local `dev` branch is up to date and rebase `dev` into your branch.
5. When you're ready create a merge request for your branch to merge into `dev`.
6. Add the new chore to the [changelog](CHANGELOG.md) and reference the created merge request.
7. Discuss and fix any issues the reviewers might see in your code.
8. Wait for the chore to be merged.

Don't create your `chore` branches from any other branch than `dev` to prevent git :spaghetti:!

### Steps for implementing a new release (ONLY MAINTAINERS)

1. Make sure your local `dev` branch is up to date.
2. Create a new branch from `dev`, name it `release/vM.m.p` where `M.m.p` follows [SemVer](https://semver.org/) formatting.
3. Bump the version.
4. Fix potential last minute issues.
5. When you're ready create a merge request for your branch to merge into `prod`.
6. Update the [changelog](CHANGELOG.md) and reference the created merge request next to the version number.
7. Discuss and fix any issues the reviewers might see in your code.
8. Wait for the new release to be merged.

Don't create your `release` branches from any other branch than `dev` to prevent git :spaghetti:!

### Steps for implementing a new hotfix (ONLY MAINTAINERS)

1. Make sure your local `prod` branch is up to date.
2. Create a new branch from `prod`, name it `hotfix/#n-...` where `n` is the issue ID and end it with a short description of the hotfix.
3. Bump the version.
4. Make your changes.
5. Make sure your local `prod` branch is up to date and rebase `prod` into your branch.
6. When you're ready create a merge request for your branch to merge into `dev`.
7. Add the new hotfix to the [changelog](CHANGELOG.md) and reference the created merge request.
8. Discuss and fix any issues the reviewers might see in your code.
9. Wait for the hotfix to be merged.

Don't create your `hotfix` branches from any other branch than `prod` to prevent git :spaghetti:!

## Project Guidelines

### General

- Add a **doc.go** file to any newly added packages which consist of more than 1 file.
- Try to write your code in the `pkg` directory instead of using `internal`.

## Testing

This repository strives for a 100% total code coverage,
so try to cover as much of your code as you can in your unit tests.

When writing your tests make sure to always **BET** (**Benchmark**, **Example**, **Test**) your code.

### Naming Convention

In this project the [naming convention for Examples](https://blog.golang.org/examples) is applied to Benchmarks and Tests as well,
this ensures consistency in the test files.
