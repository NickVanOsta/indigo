#!make

# Author: Nick Van Osta
# MIT License

PKG_LIST := $(shell go list ./...)
TEST_TIMEOUT := "30s"
BENCH_TIMEOUT := "120s"

BUILD_DIR := "./build"
COVER_DIR := "./coverage"

.PHONY: all dep build clean test coverage coverhtml lint

all: init build

init: ## Initialize the project
	@git config core.hooksPath .githooks

run: build ## Run the application
	@build/*

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}
	@go vet ${PKG_LIST}

test: ## Run unit tests
	@go test -timeout ${TEST_TIMEOUT} -short ${PKG_LIST}

race: ## Run data race detector
	@go test -timeout ${TEST_TIMEOUT} -race -short ${PKG_LIST}

msan: ## Run memory sanitizer
	@go test -timeout ${TEST_TIMEOUT} -msan -short ${PKG_LIST}

benchmark: ## Run benchmark tests
	@bash ./scripts/benchmark.sh ${BENCH_TIMEOUT};

coverage: ## Generate global code coverage report
	@bash ./scripts/coverage.sh ${COVER_DIR};

coverhtml: ## Generate global code coverage report in HTML
	@bash ./scripts/coverage.sh ${COVER_DIR} html;

docker: ## Build and push the Docker configuration
	@bash ./scripts/docker.sh

build: ## Build the binary file
	@[ -d "${BUILD_DIR}" ] || mkdir ${BUILD_DIR}
	@go build -v -o ${BUILD_DIR} ${PKG_LIST}

clean: ## Remove previous build
	@rm -f ${BUILD_DIR}/* ${COVER_DIR}/*

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
