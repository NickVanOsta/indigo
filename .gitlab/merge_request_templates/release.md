# Release

[//]: # 'Add the release version to the Title above'

## Checklist

[//]: # 'This is a list of things to be checked by the reviewers'
[//]: # 'Please leave everything unchecked'

- [ ] Documentation has been added/updated where necessary.
- [ ] All texts and comments are in English.
- [ ] The [Guidelines](CONTRIBUTING.md) have been followed.
- [ ] At least one issue has been referenced.
- [ ] Merging into `dev`.
- [ ] MR is tagged correctly.
- [ ] CHANGELOG has been updated if necessary.
