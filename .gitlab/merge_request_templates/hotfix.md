# Hotfix

[//]: # 'Provide a general summary of your changes in the Title above'

Resolves #

[//]: # 'Enter the issue id in the line above, right after the #'

## Changes

[//]: # 'Describe your changes in detail'

## How to test

[//]: # 'Please describe in detail how to test your changes'
[//]: # 'Include details of your testing environment, and the tests you ran to see how your change affects other areas of the code, etc.'

## Screenshots (if appropriate)

## Checklist

[//]: # 'This is a list of things to be checked by the reviewers'
[//]: # 'Please leave everything unchecked'

- [ ] Documentation has been added/updated where necessary.
- [ ] All texts and comments are in English.
- [ ] The [Guidelines](CONTRIBUTING.md) have been followed.
- [ ] At least one issue has been referenced.
- [ ] Merging into `dev`.
- [ ] MR is tagged correctly.
- [ ] CHANGELOG has been updated if necessary.
