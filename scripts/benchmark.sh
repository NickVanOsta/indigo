#!/bin/bash

[[ ! -z "$1" ]] && timeout=$1 || timeout=0;

PKG_LIST=$(go list -f "{{.Dir}}" ./...)
(
  IFS=$'\n'
  for package in ${PKG_LIST}; do
    cd "$package";
    go test -benchmem -bench=. -timeout=$1;
  done
)
