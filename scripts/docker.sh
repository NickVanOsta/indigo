#!/bin/bash

CI_REGISTRY=registry.gitlab.com
CI_REPOSITORY=registry.gitlab.com/nickvanosta/indigo
CI_REGISTRY_USER=NickVanOsta

# login
docker login -u $([ "${1^^}" == "CI" ] && gitlab-ci-token || $CI_REGISTRY_USER) $CI_REGISTRY

# build docker image
docker build -t $CI_REPOSITORY .

# push docker image
docker push $CI_REPOSITORY

exit 0
