#!/bin/bash

# Create coverage directory if it does not exist
[ ! -d "$1" ] && mkdir $1

PKG_LIST=$(go list ./...)
for package in ${PKG_LIST}; do
  go test -covermode=count -coverprofile "$1/${package##*/}.cov" "$package" ;
done

echo 'mode: count' > "$1/coverage.cov"
tail -q -n +2 $1/*.cov >> "$1/coverage.cov"
go tool cover -func="$1/coverage.cov"

[[ ! -z "$2" && "${2^^}" == "HTML" ]] && go tool cover -html="$1/coverage.cov" -o "$1/coverage.html"

exit 0
