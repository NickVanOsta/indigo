package mathengine

import (
	"fmt"
	"math"
	"testing"
)

// === EXAMPLES ===

func ExampleZeroVector2() {
	fmt.Println(ZeroVector2())
	//Output:
	// [0 0]
}

func ExampleUnitXVector2() {
	fmt.Println(UnitXVector2())
	//Output:
	// [1 0]
}

func ExampleUnitYVector2() {
	fmt.Println(UnitYVector2())
	//Output:
	// [0 1]
}

func ExampleVector2_Add() {
	u := Vector2{2, 3}
	v := Vector2{4, 9}
	w := Vector2{8, 27}

	u.Add(&v, &w)
	fmt.Println(u)
	// Output:
	// [14 39]
}

func ExampleVector2_FAdd() {
	u := Vector2{2, 3}
	v := Vector2{4, 9}
	w := Vector2{8, 27}

	fmt.Println(u.FAdd(&v, &w))
	// Output:
	// [14 39]
}

func ExampleVector2_Subtract() {
	u := Vector2{2, 3}
	v := Vector2{4, 9}
	w := Vector2{8, 27}

	u.Subtract(&v, &w)
	fmt.Println(u)
	// Output:
	// [-10 -33]
}

func ExampleVector2_FSubtract() {
	u := Vector2{2, 3}
	v := Vector2{4, 9}
	w := Vector2{8, 27}

	fmt.Println(u.FSubtract(&v, &w))
	// Output:
	// [-10 -33]
}

func ExampleVector2_Dot() {
	u := Vector2{2, 3}
	v := Vector2{4, 9}

	fmt.Println(u.Dot(&v))
	// Output:
	// 35
}

func ExampleVector2_Scale() {
	u := Vector2{2, 3}
	k := 42.

	u.Scale(k)
	fmt.Println(u)
	// Output:
	// [84 126]
}

func ExampleVector2_FScale() {
	u := Vector2{2, 3}
	k := 42.

	fmt.Println(u.FScale(k))
	// Output:
	// [84 126]
}

func ExampleVector2_NormSquared() {
	u := Vector2{3, 4}

	fmt.Println(u.NormSquared())
	// Output:
	// 25
}

func ExampleVector2_Norm() {
	u := Vector2{3, 4}

	fmt.Println(u.Norm())
	// Output:
	// 5
}

func ExampleVector2_Normalize() {
	u := Vector2{15, 20}

	u.Normalize()
	fmt.Println(u)
	// Output:
	// [0.6 0.8]
}

func ExampleVector2_FNormalize() {
	u := Vector2{15, 20}

	fmt.Println(u.FNormalize())
	// Output:
	// [0.6 0.8]
}

func ExampleVector2_Rotate() {
	u := Vector2{3, 0}
	angle := math.Pi * .5

	u.Rotate(angle)
	fmt.Println(u)
	//Output:
	// [1.8369701987210272e-16 3]
}

func ExampleVector2_FRotate() {
	u := Vector2{3, 0}
	angle := math.Pi * .5

	fmt.Println(u.FRotate(angle))
	//Output:
	// [1.8369701987210272e-16 3]
}

func ExampleVector2_RotateAndTranslate() {
	u := Vector2{3, 0}
	angle := math.Pi * .5
	translation := Vector2{1, 1}

	u.RotateAndTranslate(angle, &translation)
	fmt.Println(u)
	//Output:
	// [1.0000000000000002 4]
}

func ExampleVector2_FRotateAndTranslate() {
	u := Vector2{3, 0}
	angle := math.Pi * .5
	translation := Vector2{1, 1}

	fmt.Println(u.FRotateAndTranslate(angle, &translation))
	//Output:
	// [1.0000000000000002 4]
}

// === TESTS ===

func TestZeroVector2(t *testing.T) {
	w := Vector2{0, 0}
	if o := *ZeroVector2(); o != w {
		t.Errorf("Expected %v, but got %v.\n", w, o)
	}
}

func TestUnitXVector2(t *testing.T) {
	w := Vector2{1, 0}
	if ex := *UnitXVector2(); ex != w {
		t.Errorf("Expected %v, but got %v.\n", w, ex)
	}
}

func TestUnitYVector2(t *testing.T) {
	w := Vector2{0, 1}
	if ey := *UnitYVector2(); ey != w {
		t.Errorf("Expected %v, but got %v.\n", w, ey)
	}
}

func TestVector2_Add(t *testing.T) {
	ex := Vector2{1, 0}
	ey := Vector2{0, 1}

	t.Run("Adds 2 vectors correctly", func(t *testing.T) {
		u := Vector2{3, 3}
		w := Vector2{3, 4}
		if u.Add(&ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})

	t.Run("Adds 3 vectors correctly", func(t *testing.T) {
		u := Vector2{3, 3}
		w := Vector2{4, 4}
		if u.Add(&ex, &ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})
}

func TestVector2_FAdd(t *testing.T) {
	ex := Vector2{1, 0}
	ey := Vector2{0, 1}
	u := Vector2{3, 3}

	t.Run("Adds 2 vectors correctly", func(t *testing.T) {
		w := Vector2{3, 4}
		if v := *u.FAdd(&ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Adds 3 vectors correctly", func(t *testing.T) {
		w := Vector2{4, 4}
		if v := *u.FAdd(&ex, &ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Creates a copy when no vectors are given", func(t *testing.T) {
		v := *u.FAdd()
		if v != u {
			t.Errorf("Expected %v, but got %v.\n", u, v)
		}
		if &v == &u {
			t.Errorf("Expected not %v, but got %v.\n", &u, &v)
		}
	})
}

func TestVector2_Subtract(t *testing.T) {
	ex := Vector2{1, 0}
	ey := Vector2{0, 1}

	t.Run("Subtracts 2 vectors correctly", func(t *testing.T) {
		u := Vector2{3, 3}
		w := Vector2{3, 2}
		if u.Subtract(&ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})

	t.Run("Subtracts 3 vectors correctly", func(t *testing.T) {
		u := Vector2{3, 3}
		w := Vector2{2, 2}
		if u.Subtract(&ex, &ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})
}

func TestVector2_FSubtract(t *testing.T) {
	ex := Vector2{1, 0}
	ey := Vector2{0, 1}
	u := Vector2{3, 3}

	t.Run("Subtracts 2 vectors correctly", func(t *testing.T) {
		w := Vector2{3, 2}
		if v := *u.FSubtract(&ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Subtracts 3 vectors correctly", func(t *testing.T) {
		w := Vector2{2, 2}
		if v := *u.FSubtract(&ex, &ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Creates a copy when no vectors are given", func(t *testing.T) {
		v := *u.FSubtract()
		if v != u {
			t.Errorf("Expected %v, but got %v.\n", u, v)
		}
		if &v == &u {
			t.Errorf("Expected not %v, but got %v.\n", &u, &v)
		}
	})
}

func TestVector2_Dot(t *testing.T) {
	u := Vector2{2, -3}
	v := Vector2{5, 2}

	t.Run("Calculates the correct dot product of the given vectors", func(t *testing.T) {
		w := 4.
		if x := u.Dot(&v); x != w {
			t.Errorf("Expected %v, but got %v.\n", w, x)
		}
	})
}

func TestVector2_Scale(t *testing.T) {
	k := 5.

	t.Run("Scales the vector with a factor >1 correctly", func(t *testing.T) {
		u := Vector2{1, 1}
		w := Vector2{5, 5}
		if u.Scale(k); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})

	t.Run("Scales the vector with a factor <1 correctly", func(t *testing.T) {
		u := Vector2{1, 1}
		w := Vector2{.2, .2}
		if u.Scale(1 / k); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})
}

func TestVector2_FScale(t *testing.T) {
	u := Vector2{1, 1}
	k := 5.

	t.Run("Scales the vector with a factor >1 correctly", func(t *testing.T) {
		w := Vector2{5, 5}
		if v := *u.FScale(k); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Scales the vector with a factor <1 correctly", func(t *testing.T) {
		w := Vector2{.2, .2}
		if v := *u.FScale(1 / k); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})
}

func TestVector2_NormSquared(t *testing.T) {
	u := Vector2{3, 4}

	t.Run("Calculates the squared length of the vector correctly", func(t *testing.T) {
		w := 25.
		if x := u.NormSquared(); x != w {
			t.Errorf("Expected %v, but got %v.\n", w, x)
		}
	})
}

func TestVector2_Norm(t *testing.T) {
	u := Vector2{3, 4}

	t.Run("Calculates the length of the vector correctly", func(t *testing.T) {
		w := 5.
		if x := u.Norm(); x != w {
			t.Errorf("Expected %v, but got %v.\n", w, x)
		}
	})
}

func TestVector2_Normalize(t *testing.T) {
	u := Vector2{3, 4}

	t.Run("Normalizes the vector correctly", func(t *testing.T) {
		w := Vector2{.6, .8}
		if u.Normalize(); u.X-w.X > 0.0001 || u.Y-w.Y > 0.0001 {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})
}

func TestVector2_FNormalize(t *testing.T) {
	u := Vector2{3, 4}

	t.Run("Normalizes the vector correctly", func(t *testing.T) {
		w := Vector2{.6, .8}
		if v := *u.FNormalize(); v.X-w.X > 0.0001 || v.Y-w.Y > 0.0001 {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})
}

func TestVector2_Rotate(t *testing.T) {
	u := Vector2{3, 0}
	angle := math.Pi * .5

	w := Vector2{0, 3}
	if u.Rotate(angle); u.X-w.X > 0.0001 || u.Y-w.Y > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, u)
	}
}

func TestVector2_FRotate(t *testing.T) {
	u := Vector2{3, 0}
	angle := math.Pi * .5

	w := Vector2{0, 3}
	if v := *u.FRotate(angle); v.X-w.X > 0.0001 || v.Y-w.Y > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, v)
	}
}

func TestVector2_RotateAndTranslate(t *testing.T) {
	u := Vector2{3, 0}
	angle := math.Pi * .5
	translation := Vector2{1, 1}

	w := Vector2{1, 4}
	if u.RotateAndTranslate(angle, &translation); u.X-w.X > 0.0001 || u.Y-w.Y > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, u)
	}
}

func TestVector2_FRotateAndTranslate(t *testing.T) {
	u := Vector2{3, 0}
	angle := math.Pi * .5
	translation := Vector2{1, 1}

	w := Vector2{1, 4}
	if v := *u.FRotateAndTranslate(angle, &translation); v.X-w.X > 0.0001 || v.Y-w.Y > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, v)
	}
}

// === BENCHMARKS ===

func BenchmarkZeroVector2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ZeroVector2()
	}
}

func BenchmarkUnitXVector2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		UnitXVector2()
	}
}

func BenchmarkUnitYVector2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		UnitYVector2()
	}
}

func BenchmarkVector2_Add(b *testing.B) {
	u := &Vector2{3, 4}

	b.Run("Add 2 vectors", func(b *testing.B) {
		v := Vector2{6, 8}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Add(&v)
		}
	})

	b.Run("Add 8 vectors", func(b *testing.B) {
		v := []*Vector2{{6, 8}, {9, 12}, {12, 16}, {15, 20}, {18, 24}, {21, 28}, {24, 32}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Add(v...)
		}
	})

	b.Run("Add no vectors", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Add()
		}
	})
}

func BenchmarkVector2_FAdd(b *testing.B) {
	u := &Vector2{3, 4}

	b.Run("Add 2 vectors", func(b *testing.B) {
		v := Vector2{6, 8}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FAdd(&v)
		}
	})

	b.Run("Add 8 vectors", func(b *testing.B) {
		v := []*Vector2{{6, 8}, {9, 12}, {12, 16}, {15, 20}, {18, 24}, {21, 28}, {24, 32}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FAdd(v...)
		}
	})

	b.Run("Copy a vector", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FAdd()
		}
	})
}

func BenchmarkVector2_Subtract(b *testing.B) {
	u := &Vector2{3, 4}

	b.Run("Subtract 2 vectors", func(b *testing.B) {
		v := Vector2{6, 8}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Subtract(&v)
		}
	})

	b.Run("Subtract 8 vectors", func(b *testing.B) {
		v := []*Vector2{{6, 8}, {9, 12}, {12, 16}, {15, 20}, {18, 24}, {21, 28}, {24, 32}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Subtract(v...)
		}
	})

	b.Run("Subtract no vectors", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Subtract()
		}
	})
}

func BenchmarkVector2_FSubtract(b *testing.B) {
	u := &Vector2{3, 4}

	b.Run("Subtract 2 vectors", func(b *testing.B) {
		v := Vector2{6, 8}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FSubtract(&v)
		}
	})

	b.Run("Subtract 8 vectors", func(b *testing.B) {
		v := []*Vector2{{6, 8}, {9, 12}, {12, 16}, {15, 20}, {18, 24}, {21, 28}, {24, 32}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FSubtract(v...)
		}
	})

	b.Run("Copy a vector", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FSubtract()
		}
	})
}

func BenchmarkVector2_Dot(b *testing.B) {
	u := &Vector2{3, 4}
	v := &Vector2{6, 8}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Dot(v)
	}
}

func BenchmarkVector2_Scale(b *testing.B) {
	u := &Vector2{3, 4}
	k := 3.14159265359

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Scale(k)
	}
}

func BenchmarkVector2_FScale(b *testing.B) {
	u := &Vector2{3, 4}
	k := 3.14159265359

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FScale(k)
	}
}

func BenchmarkVector2_NormSquared(b *testing.B) {
	u := &Vector2{3, 4}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.NormSquared()
	}
}

func BenchmarkVector2_Norm(b *testing.B) {
	u := &Vector2{3, 4}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Norm()
	}
}

func BenchmarkVector2_Normalize(b *testing.B) {
	u := &Vector2{3, 4}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Normalize()
	}
}

func BenchmarkVector2_FNormalize(b *testing.B) {
	u := &Vector2{3, 4}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FNormalize()
	}
}

func BenchmarkVector2_Rotate(b *testing.B) {
	u := &Vector2{3, 4}
	angle := 1.

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Rotate(angle)
	}
}

func BenchmarkVector2_FRotate(b *testing.B) {
	u := &Vector2{3, 4}
	angle := 1.

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FRotate(angle)
	}
}

func BenchmarkVector2_RotateAndTranslate(b *testing.B) {
	u := &Vector2{3, 4}
	angle := 1.
	translation := &Vector2{3, 2}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.RotateAndTranslate(angle, translation)
	}
}

func BenchmarkVector2_FRotateAndTranslate(b *testing.B) {
	u := &Vector2{3, 4}
	angle := 1.
	translation := &Vector2{3, 2}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FRotateAndTranslate(angle, translation)
	}
}
