package mathengine

import (
	"fmt"
	"testing"
)

// === EXAMPLES ===

func ExampleIdentityMatrix3() {
	fmt.Println(IdentityMatrix3())
	//Output:
	// [[1 0 0] [0 1 0] [0 0 1]]
}

func ExampleMatrix3_Add() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	p := Matrix3{
		3, 2, 1,
		3, 2, 1,
		3, 2, 1,
	}

	m.Add(&n, &p)
	fmt.Println(m)
	//Output:
	// [[5 5 5] [6 6 6] [7 7 7]]
}

func ExampleMatrix3_FAdd() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	p := Matrix3{
		3, 2, 1,
		3, 2, 1,
		3, 2, 1,
	}

	fmt.Println(m.FAdd(&n, &p))
	//Output:
	// [[5 5 5] [6 6 6] [7 7 7]]
}

func ExampleMatrix3_Subtract() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	p := Matrix3{
		3, 2, 1,
		3, 2, 1,
		3, 2, 1,
	}

	m.Subtract(&n, &p)
	fmt.Println(m)
	//Output:
	// [[-3 -3 -3] [-2 -2 -2] [-1 -1 -1]]
}

func ExampleMatrix3_FSubtract() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	p := Matrix3{
		3, 2, 1,
		3, 2, 1,
		3, 2, 1,
	}

	fmt.Println(m.FSubtract(&n, &p))
	//Output:
	// [[-3 -3 -3] [-2 -2 -2] [-1 -1 -1]]
}

func ExampleMatrix3_MultiplyScalar() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	k := 2.

	m.MultiplyScalar(k)
	fmt.Println(m)
	//Output:
	// [[2 2 2] [4 4 4] [6 6 6]]
}

func ExampleMatrix3_FMultiplyScalar() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	k := 2.

	fmt.Println(m.FMultiplyScalar(k))
	//Output:
	// [[2 2 2] [4 4 4] [6 6 6]]
}

func ExampleMatrix3_Multiply() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	p := Matrix3{
		3, 2, 1,
		3, 2, 1,
		3, 2, 1,
	}

	m.Multiply(&n, &p)
	fmt.Println(m)
	//Output:
	// [[54 36 18] [108 72 36] [162 108 54]]
}

func ExampleMatrix3_FMultiply() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	p := Matrix3{
		3, 2, 1,
		3, 2, 1,
		3, 2, 1,
	}

	fmt.Println(m.FMultiply(&n, &p))
	//Output:
	// [[54 36 18] [108 72 36] [162 108 54]]
}

func ExampleMatrix3_Invert_invertible() {
	m := Matrix3{
		2, 1, 1,
		3, 2, 1,
		2, 1, 2,
	}

	m.Invert()
	fmt.Println(m)
	//Output:
	// [[3 -1 -1] [-4 2 1] [-1 0 1]]
}

func ExampleMatrix3_Invert_uninvertible() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}

	m.Invert()
	fmt.Println(m)
	//Output:
	// [[1 1 1] [2 2 2] [3 3 3]]
}

func ExampleMatrix3_FInvert_invertible() {
	m := Matrix3{
		2, 1, 1,
		3, 2, 1,
		2, 1, 2,
	}

	fmt.Println(m.FInvert())
	//Output:
	// [[3 -1 -1] [-4 2 1] [-1 0 1]]
}

func ExampleMatrix3_FInvert_uninvertible() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}

	fmt.Println(m.FInvert())
	//Output:
	// <nil>
}

func ExampleMatrix3_Transpose() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}

	m.Transpose()
	fmt.Println(m)
	//Output:
	// [[1 2 3] [1 2 3] [1 2 3]]
}

func ExampleMatrix3_FTranspose() {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}

	fmt.Println(m.FTranspose())
	//Output:
	// [[1 2 3] [1 2 3] [1 2 3]]
}

// === TESTS ===

func TestIdentityMatrix3(t *testing.T) {
	t.Run("Creates the 3 by 3 identity matrix correctly", func(t *testing.T) {
		w := Matrix3{1, 0, 0, 0, 1, 0, 0, 0, 1}
		if i := *IdentityMatrix3(); i != w {
			t.Errorf("Expected %v, but got %v.\n", w, i)
		}
	})
}

func TestMatrix3_Add(t *testing.T) {
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	i := Matrix3{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,
	}

	t.Run("Adds 2 matrices correctly", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}
		w := Matrix3{
			2, 3, 4,
			3, 4, 5,
			4, 5, 6,
		}

		if m.Add(&n); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Adds 3 matrices correctly", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}
		w := Matrix3{
			3, 3, 4,
			3, 5, 5,
			4, 5, 7,
		}

		if m.Add(&n, &i); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})
}

func TestMatrix3_FAdd(t *testing.T) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	i := Matrix3{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,
	}

	t.Run("Adds 2 matrices correctly", func(t *testing.T) {
		w := Matrix3{
			2, 3, 4,
			3, 4, 5,
			4, 5, 6,
		}

		if y := *m.FAdd(&n); y != w {
			t.Errorf("Expected %v, but got %v.\n", w, y)
		}
	})

	t.Run("Adds 3 matrices correctly", func(t *testing.T) {
		w := Matrix3{
			3, 3, 4,
			3, 5, 5,
			4, 5, 7,
		}

		if y := *m.FAdd(&n, &i); y != w {
			t.Errorf("Expected %v, but got %v.\n", w, y)
		}
	})

	t.Run("Creates a copy when no matrices are given", func(t *testing.T) {
		y := *m.FAdd()
		if y != m {
			t.Errorf("Expected %v, but got %v.\n", m, y)
		}
		if &y == &m {
			t.Errorf("Expected not %v, but got %v.\n", &m, &y)
		}
	})
}

func TestMatrix3_Subtract(t *testing.T) {
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	i := Matrix3{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,
	}

	t.Run("Subtracts 2 matrices correctly", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}
		w := Matrix3{
			0, -1, -2,
			1, 0, -1,
			2, 1, 0,
		}

		if m.Subtract(&n); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Subtracts 3 matrices correctly", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}
		w := Matrix3{
			-1, -1, -2,
			1, -1, -1,
			2, 1, -1,
		}

		if m.Subtract(&n, &i); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})
}

func TestMatrix3_FSubtract(t *testing.T) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	i := Matrix3{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,
	}

	t.Run("Subtracts 2 matrices correctly", func(t *testing.T) {
		w := Matrix3{
			0, -1, -2,
			1, 0, -1,
			2, 1, 0,
		}

		if y := *m.FSubtract(&n); y != w {
			t.Errorf("Expected %v, but got %v.\n", w, y)
		}
	})

	t.Run("Subtracts 3 matrices correctly", func(t *testing.T) {
		w := Matrix3{
			-1, -1, -2,
			1, -1, -1,
			2, 1, -1,
		}

		if y := *m.FSubtract(&n, &i); y != w {
			t.Errorf("Expected %v, but got %v.\n", w, y)
		}
	})

	t.Run("Creates a copy when no matrices are given", func(t *testing.T) {
		y := *m.FSubtract()
		if y != m {
			t.Errorf("Expected %v, but got %v.\n", m, y)
		}
		if &y == &m {
			t.Errorf("Expected not %v, but got %v.\n", &m, &y)
		}
	})
}

func TestMatrix3_MultiplyScalar(t *testing.T) {
	k := 5.

	t.Run("Scales the matrix with a factor >1 correctly", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}
		w := Matrix3{
			5, 5, 5,
			10, 10, 10,
			15, 15, 15,
		}

		if m.MultiplyScalar(k); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Scales the matrix with a factor <1 correctly", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}
		w := Matrix3{
			.2, .2, .2,
			.4, .4, .4,
			.6, .6, .6,
		}

		m.MultiplyScalar(1 / k)
		for i, x := range m {
			if x-w[i] > 0.0001 {
				t.Errorf("Expected %v, but got %v.\n", w, m)
			}
		}
	})
}

func TestMatrix3_FMultiplyScalar(t *testing.T) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	k := 5.

	t.Run("Scales the matrix with a factor >1 correctly", func(t *testing.T) {
		w := Matrix3{
			5, 5, 5,
			10, 10, 10,
			15, 15, 15,
		}

		if n := *m.FMultiplyScalar(k); n != w {
			t.Errorf("Expected %v, but got %v.\n", w, n)
		}
	})

	t.Run("Scales the matrix with a factor <1 correctly", func(t *testing.T) {
		w := Matrix3{
			.2, .2, .2,
			.4, .4, .4,
			.6, .6, .6,
		}

		n := *m.FMultiplyScalar(1 / k)
		for i, x := range n {
			if x-w[i] > 0.0001 {
				t.Errorf("Expected %v, but got %v.\n", w, n)
			}
		}
	})
}

func TestMatrix3_Multiply(t *testing.T) {
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	i := Matrix3{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,
	}

	t.Run("Multiplies 2 matrices correctly", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}
		w := Matrix3{
			3, 6, 9,
			6, 12, 18,
			9, 18, 27,
		}

		if m.Multiply(&n); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Multiplies 3 matrices correctly", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}
		w := Matrix3{
			3, 6, 9,
			6, 12, 18,
			9, 18, 27,
		}

		if m.Multiply(&n, &i); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})
}

func TestMatrix3_FMultiply(t *testing.T) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	n := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}
	i := Matrix3{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,
	}

	t.Run("Multiplies 2 matrices correctly", func(t *testing.T) {
		w := Matrix3{
			3, 6, 9,
			6, 12, 18,
			9, 18, 27,
		}

		if n := *m.FMultiply(&n); n != w {
			t.Errorf("Expected %v, but got %v.\n", w, n)
		}
	})

	t.Run("Multiplies 3 matrices correctly", func(t *testing.T) {
		w := Matrix3{
			3, 6, 9,
			6, 12, 18,
			9, 18, 27,
		}

		if n := *m.FMultiply(&n, &i); n != w {
			t.Errorf("Expected %v, but got %v.\n", w, n)
		}
	})

	t.Run("Creates a copy when no matrices are given", func(t *testing.T) {
		y := *m.FMultiply()
		if y != m {
			t.Errorf("Expected %v, but got %v.\n", m, y)
		}
		if &y == &m {
			t.Errorf("Expected not %v, but got %v.\n", &m, &y)
		}
	})
}

func TestMatrix3_Invert(t *testing.T) {
	t.Run("Inverts an invertible matrix correctly", func(t *testing.T) {
		m := Matrix3{
			2, 1, 1,
			3, 2, 1,
			2, 1, 2,
		}
		w := Matrix3{
			3, -1, -1,
			-4, 2, 1,
			-1, 0, 1,
		}

		if m.Invert(); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Returns nil when the matrix is not invertible", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}
		w := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}

		if m.Invert(); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})
}

func TestMatrix3_FInvert(t *testing.T) {
	t.Run("Inverts an invertible matrix correctly", func(t *testing.T) {
		m := Matrix3{
			2, 1, 1,
			3, 2, 1,
			2, 1, 2,
		}
		w := Matrix3{
			3, -1, -1,
			-4, 2, 1,
			-1, 0, 1,
		}

		if n := *m.FInvert(); n != w {
			t.Errorf("Expected %v, but got %v.\n", w, n)
		}
	})

	t.Run("Returns nil when the matrix is not invertible", func(t *testing.T) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}

		if n := m.FInvert(); n != nil {
			t.Errorf("Expected %v, but got %v.\n", nil, n)
		}
	})
}

func TestMatrix3_Transpose(t *testing.T) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	w := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}

	if m.Transpose(); m != w {
		t.Errorf("Expected %v, but got %v.\n", w, m)
	}
}

func TestMatrix3_FTranspose(t *testing.T) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	w := Matrix3{
		1, 2, 3,
		1, 2, 3,
		1, 2, 3,
	}

	if n := *m.FTranspose(); n != w {
		t.Errorf("Expected %v, but got %v.\n", w, n)
	}
}

// === BENCHMARKS ===

func BenchmarkIdentityMatrix3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		IdentityMatrix3()
	}
}

func BenchmarkMatrix3_Add(b *testing.B) {
	m := Matrix3{1, 1, 1, 2, 2, 2, 3, 3, 3}

	b.Run("Add 2 matrices", func(b *testing.B) {
		n := Matrix3{2, 2, 2, 4, 4, 4, 6, 6, 6}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Add(&n)
		}
	})

	b.Run("Add 8 matrices", func(b *testing.B) {
		n := []*Matrix3{
			{2, 2, 2, 4, 4, 4, 6, 6, 6},
			{3, 3, 3, 6, 6, 6, 9, 9, 9},
			{4, 4, 4, 8, 8, 8, 12, 12, 12},
			{5, 5, 5, 10, 10, 10, 15, 15, 15},
			{6, 6, 6, 12, 12, 12, 18, 18, 18},
			{7, 7, 7, 14, 14, 14, 21, 21, 21},
			{8, 8, 8, 16, 16, 16, 24, 24, 24},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Add(n...)
		}
	})

	b.Run("Add no matrices", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Add()
		}
	})
}

func BenchmarkMatrix3_FAdd(b *testing.B) {
	m := Matrix3{1, 1, 1, 2, 2, 2, 3, 3, 3}

	b.Run("Add 2 matrices", func(b *testing.B) {
		n := Matrix3{2, 2, 2, 4, 4, 4, 6, 6, 6}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FAdd(&n)
		}
	})

	b.Run("Add 8 matrices", func(b *testing.B) {
		n := []*Matrix3{
			{2, 2, 2, 4, 4, 4, 6, 6, 6},
			{3, 3, 3, 6, 6, 6, 9, 9, 9},
			{4, 4, 4, 8, 8, 8, 12, 12, 12},
			{5, 5, 5, 10, 10, 10, 15, 15, 15},
			{6, 6, 6, 12, 12, 12, 18, 18, 18},
			{7, 7, 7, 14, 14, 14, 21, 21, 21},
			{8, 8, 8, 16, 16, 16, 24, 24, 24},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FAdd(n...)
		}
	})

	b.Run("Copy a matrix", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FAdd()
		}
	})
}

func BenchmarkMatrix3_Subtract(b *testing.B) {
	m := Matrix3{1, 1, 1, 2, 2, 2, 3, 3, 3}

	b.Run("Subtract 2 matrices", func(b *testing.B) {
		n := Matrix3{2, 2, 2, 4, 4, 4, 6, 6, 6}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Subtract(&n)
		}
	})

	b.Run("Subtract 8 matrices", func(b *testing.B) {
		n := []*Matrix3{
			{2, 2, 2, 4, 4, 4, 6, 6, 6},
			{3, 3, 3, 6, 6, 6, 9, 9, 9},
			{4, 4, 4, 8, 8, 8, 12, 12, 12},
			{5, 5, 5, 10, 10, 10, 15, 15, 15},
			{6, 6, 6, 12, 12, 12, 18, 18, 18},
			{7, 7, 7, 14, 14, 14, 21, 21, 21},
			{8, 8, 8, 16, 16, 16, 24, 24, 24},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Subtract(n...)
		}
	})

	b.Run("Subtract no matrices", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Subtract()
		}
	})
}

func BenchmarkMatrix3_FSubtract(b *testing.B) {
	m := Matrix3{1, 1, 1, 2, 2, 2, 3, 3, 3}

	b.Run("Subtract 2 matrices", func(b *testing.B) {
		n := Matrix3{2, 2, 2, 4, 4, 4, 6, 6, 6}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FSubtract(&n)
		}
	})

	b.Run("Subtract 8 matrices", func(b *testing.B) {
		n := []*Matrix3{
			{2, 2, 2, 4, 4, 4, 6, 6, 6},
			{3, 3, 3, 6, 6, 6, 9, 9, 9},
			{4, 4, 4, 8, 8, 8, 12, 12, 12},
			{5, 5, 5, 10, 10, 10, 15, 15, 15},
			{6, 6, 6, 12, 12, 12, 18, 18, 18},
			{7, 7, 7, 14, 14, 14, 21, 21, 21},
			{8, 8, 8, 16, 16, 16, 24, 24, 24},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FSubtract(n...)
		}
	})

	b.Run("Copy a matrix", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FSubtract()
		}
	})
}

func BenchmarkMatrix3_MultiplyScalar(b *testing.B) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	k := 3.141592

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.MultiplyScalar(k)
	}
}

func BenchmarkMatrix3_FMultiplyScalar(b *testing.B) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}
	k := 3.141592

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.FMultiplyScalar(k)
	}
}

func BenchmarkMatrix3_Multiply(b *testing.B) {
	m := Matrix3{1, 1, 1, 2, 2, 2, 3, 3, 3}

	b.Run("Multiply 2 matrices", func(b *testing.B) {
		n := Matrix3{2, 2, 2, 4, 4, 4, 6, 6, 6}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Multiply(&n)
		}
	})

	b.Run("Multiply 8 matrices", func(b *testing.B) {
		n := []*Matrix3{
			{2, 2, 2, 4, 4, 4, 6, 6, 6},
			{3, 3, 3, 6, 6, 6, 9, 9, 9},
			{4, 4, 4, 8, 8, 8, 12, 12, 12},
			{5, 5, 5, 10, 10, 10, 15, 15, 15},
			{6, 6, 6, 12, 12, 12, 18, 18, 18},
			{7, 7, 7, 14, 14, 14, 21, 21, 21},
			{8, 8, 8, 16, 16, 16, 24, 24, 24},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Multiply(n...)
		}
	})

	b.Run("Multiply no matrices", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Multiply()
		}
	})
}

func BenchmarkMatrix3_FMultiply(b *testing.B) {
	m := Matrix3{1, 1, 1, 2, 2, 2, 3, 3, 3}

	b.Run("Multiply 2 matrices", func(b *testing.B) {
		n := Matrix3{2, 2, 2, 4, 4, 4, 6, 6, 6}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FMultiply(&n)
		}
	})

	b.Run("Multiply 8 matrices", func(b *testing.B) {
		n := []*Matrix3{
			{2, 2, 2, 4, 4, 4, 6, 6, 6},
			{3, 3, 3, 6, 6, 6, 9, 9, 9},
			{4, 4, 4, 8, 8, 8, 12, 12, 12},
			{5, 5, 5, 10, 10, 10, 15, 15, 15},
			{6, 6, 6, 12, 12, 12, 18, 18, 18},
			{7, 7, 7, 14, 14, 14, 21, 21, 21},
			{8, 8, 8, 16, 16, 16, 24, 24, 24},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FMultiply(n...)
		}
	})

	b.Run("Copy a matrix", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FMultiply()
		}
	})
}

func BenchmarkMatrix3_Invert(b *testing.B) {
	b.Run("Invertible matrix", func(b *testing.B) {
		m := Matrix3{
			2, 1, 1,
			3, 2, 1,
			2, 1, 2,
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Invert()
		}
	})

	b.Run("Uninvertible matrix", func(b *testing.B) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Invert()
		}
	})
}

func BenchmarkMatrix3_FInvert(b *testing.B) {
	b.Run("Invertible matrix", func(b *testing.B) {
		m := Matrix3{
			2, 1, 1,
			3, 2, 1,
			2, 1, 2,
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FInvert()
		}
	})

	b.Run("Uninvertible matrix", func(b *testing.B) {
		m := Matrix3{
			1, 1, 1,
			2, 2, 2,
			3, 3, 3,
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FInvert()
		}
	})
}

func BenchmarkMatrix3_Transpose(b *testing.B) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.Transpose()
	}
}

func BenchmarkMatrix3_FTranspose(b *testing.B) {
	m := Matrix3{
		1, 1, 1,
		2, 2, 2,
		3, 3, 3,
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.FTranspose()
	}
}
