package mathengine

import "fmt"

/*
Matrix3 represents a 3x3 matrix used for transformations and translations.
*/
type Matrix3 [9]float64

func (m Matrix3) String() string {
	return fmt.Sprintf("[[%v %v %v] [%v %v %v] [%v %v %v]]", m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8])
}

/*
IdentityMatrix3 creates the 3x3 identity matrix, which is filled with zeroes except for the main diagonal which conatains ones.
*/
func IdentityMatrix3() *Matrix3 {
	return &Matrix3{
		1, 0, 0,
		0, 1, 0,
		0, 0, 1,
	}
}

/*
Add adds the given matrices to the calling matrix.

The calling matrix itself is modified and overwritten.
If a functional operation is needed, use m.FAdd(matrices).
*/
func (m *Matrix3) Add(matrices ...*Matrix3) {
	for _, n := range matrices {
		m[0] += n[0]
		m[1] += n[1]
		m[2] += n[2]

		m[3] += n[3]
		m[4] += n[4]
		m[5] += n[5]

		m[6] += n[6]
		m[7] += n[7]
		m[8] += n[8]
	}
}

/*
FAdd adds the given matrices to the calling matrix.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Add(matrices).
*/
func (m Matrix3) FAdd(matrices ...*Matrix3) *Matrix3 {
	for _, n := range matrices {
		m[0] += n[0]
		m[1] += n[1]
		m[2] += n[2]

		m[3] += n[3]
		m[4] += n[4]
		m[5] += n[5]

		m[6] += n[6]
		m[7] += n[7]
		m[8] += n[8]
	}

	return &m
}

/*
Subtract subtracts the given matrices from the calling matrix.

The calling matrix itself is modified and overwritten.
If a functional operation is needed, use m.FSubtract(matrices).
*/
func (m *Matrix3) Subtract(matrices ...*Matrix3) {
	for _, n := range matrices {
		m[0] -= n[0]
		m[1] -= n[1]
		m[2] -= n[2]

		m[3] -= n[3]
		m[4] -= n[4]
		m[5] -= n[5]

		m[6] -= n[6]
		m[7] -= n[7]
		m[8] -= n[8]
	}
}

/*
FSubtract subtracts the given matrices from the calling matrix.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Subtract(matrices).
*/
func (m Matrix3) FSubtract(matrices ...*Matrix3) *Matrix3 {
	for _, n := range matrices {
		m[0] -= n[0]
		m[1] -= n[1]
		m[2] -= n[2]

		m[3] -= n[3]
		m[4] -= n[4]
		m[5] -= n[5]

		m[6] -= n[6]
		m[7] -= n[7]
		m[8] -= n[8]
	}

	return &m
}

/*
MultiplyScalar multiplies the calling matrix by a the given scalar.

The calling matrix itself is modified and overwritten.
If a functional operation is needed, use m.FMultiplyScalar(k).
*/
func (m *Matrix3) MultiplyScalar(k float64) {
	m[0] *= k
	m[1] *= k
	m[2] *= k

	m[3] *= k
	m[4] *= k
	m[5] *= k

	m[6] *= k
	m[7] *= k
	m[8] *= k
}

/*
FMultiplyScalar multiplies the calling matrix by a the given scalar.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.MultiplyScalar(k).
*/
func (m Matrix3) FMultiplyScalar(k float64) *Matrix3 {
	m[0] *= k
	m[1] *= k
	m[2] *= k

	m[3] *= k
	m[4] *= k
	m[5] *= k

	m[6] *= k
	m[7] *= k
	m[8] *= k

	return &m
}

/*
Multiply multiplies the calling matrix by the given matrices.

The calling matrix itself is modified and overwritten.
If a functional operation is needed, use m.FMultiply(matrices).
*/
func (m *Matrix3) Multiply(matrices ...*Matrix3) {
	for _, n := range matrices {
		m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8] =
			m[0]*n[0]+m[1]*n[3]+m[2]*n[6], m[0]*n[1]+m[1]*n[4]+m[2]*n[7], m[0]*n[2]+m[1]*n[5]+m[2]*n[8],
			m[3]*n[0]+m[4]*n[3]+m[5]*n[6], m[3]*n[1]+m[4]*n[4]+m[5]*n[7], m[3]*n[2]+m[4]*n[5]+m[5]*n[8],
			m[6]*n[0]+m[7]*n[3]+m[8]*n[6], m[6]*n[1]+m[7]*n[4]+m[8]*n[7], m[6]*n[2]+m[7]*n[5]+m[8]*n[8]
	}
}

/*
FMultiply multiplies the calling matrix by the given matrices.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Multiply(matrices).
*/
func (m Matrix3) FMultiply(matrices ...*Matrix3) *Matrix3 {
	for _, n := range matrices {
		m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8] =
			m[0]*n[0]+m[1]*n[3]+m[2]*n[6], m[0]*n[1]+m[1]*n[4]+m[2]*n[7], m[0]*n[2]+m[1]*n[5]+m[2]*n[8],
			m[3]*n[0]+m[4]*n[3]+m[5]*n[6], m[3]*n[1]+m[4]*n[4]+m[5]*n[7], m[3]*n[2]+m[4]*n[5]+m[5]*n[8],
			m[6]*n[0]+m[7]*n[3]+m[8]*n[6], m[6]*n[1]+m[7]*n[4]+m[8]*n[7], m[6]*n[2]+m[7]*n[5]+m[8]*n[8]
	}

	return &m
}

/*
Invert inverts the calling matrix, does nothing when the calling matrix is not invertible.

The calling matrix itself is modified and overwritten, unless it's not invertible.
If a functional operation is needed, use m.FInvert(matrices).
*/
func (m *Matrix3) Invert() {
	k := m[0]*(m[4]*m[8]-m[5]*m[7]) + m[1]*(m[5]*m[6]-m[3]*m[8]) + m[2]*(m[3]*m[7]-m[4]*m[6])

	if k == 0 {
		return
	}

	k = 1 / k
	m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8] =
		k*(m[4]*m[8]-m[5]*m[7]), k*(m[2]*m[7]-m[1]*m[8]), k*(m[1]*m[5]-m[4]*m[2]),
		k*(m[5]*m[6]-m[3]*m[8]), k*(m[0]*m[8]-m[2]*m[6]), k*(m[2]*m[3]-m[0]*m[5]),
		k*(m[3]*m[7]-m[4]*m[6]), k*(m[1]*m[6]-m[0]*m[7]), k*(m[0]*m[4]-m[1]*m[3])
}

/*
FInvert inverts the calling matrix, returns nil when the calling matrix is not invertible.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Invert(matrices).
*/
func (m Matrix3) FInvert() *Matrix3 {
	k := m[0]*(m[4]*m[8]-m[5]*m[7]) + m[1]*(m[5]*m[6]-m[3]*m[8]) + m[2]*(m[3]*m[7]-m[4]*m[6])

	if k == 0 {
		return nil
	}

	k = 1 / k
	m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7], m[8] =
		k*(m[4]*m[8]-m[5]*m[7]), k*(m[2]*m[7]-m[1]*m[8]), k*(m[1]*m[5]-m[4]*m[2]),
		k*(m[5]*m[6]-m[3]*m[8]), k*(m[0]*m[8]-m[2]*m[6]), k*(m[2]*m[3]-m[0]*m[5]),
		k*(m[3]*m[7]-m[4]*m[6]), k*(m[1]*m[6]-m[0]*m[7]), k*(m[0]*m[4]-m[1]*m[3])

	return &m
}

/*
Transpose transposes the calling matrix, switching its rows and columns.

The calling matrix itself is modified and overwritten.
If an in-place operation is needed, use m.Transpose(matrices).
*/
func (m *Matrix3) Transpose() {
	m[1], m[2], m[3], m[5], m[6], m[7] = m[3], m[6], m[1], m[7], m[2], m[5]
}

/*
FTranspose transposes the calling matrix, switching its rows and columns.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Transpose(matrices).
*/
func (m Matrix3) FTranspose() *Matrix3 {
	m[1], m[2], m[3], m[5], m[6], m[7] = m[3], m[6], m[1], m[7], m[2], m[5]
	return &m
}
