package mathengine

import "fmt"

/*
Matrix2 represents a 2x2 matrix used for transformations and translations in 2-dimensional space.
*/
type Matrix2 [4]float64

func (m Matrix2) String() string {
	return fmt.Sprintf("[[%v %v] [%v %v]]", m[0], m[1], m[2], m[3])
}

/*
IdentityMatrix2 creates the 2x2 identity matrix, which is filled with zeroes except for the main diagonal which conatains ones.
*/
func IdentityMatrix2() *Matrix2 {
	return &Matrix2{
		1, 0,
		0, 1,
	}
}

/*
Add adds the given matrices to the calling matrix.

The calling matrix itself is modified and overwritten.
If a functional operation is needed, use m.FAdd(matrices).
*/
func (m *Matrix2) Add(matrices ...*Matrix2) {
	for _, n := range matrices {
		m[0] += n[0]
		m[1] += n[1]

		m[2] += n[2]
		m[3] += n[3]
	}
}

/*
FAdd adds the given matrices to the calling matrix.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Add(matrices).
*/
func (m Matrix2) FAdd(matrices ...*Matrix2) *Matrix2 {
	for _, n := range matrices {
		m[0] += n[0]
		m[1] += n[1]

		m[2] += n[2]
		m[3] += n[3]
	}

	return &m
}

/*
Subtract subtracts the given matrices from the calling matrix.

The calling matrix itself is modified and overwritten.
If a functional operation is needed, use m.FSubtract(matrices).
*/
func (m *Matrix2) Subtract(matrices ...*Matrix2) {
	for _, n := range matrices {
		m[0] -= n[0]
		m[1] -= n[1]

		m[2] -= n[2]
		m[3] -= n[3]
	}
}

/*
FSubtract subtracts the given matrices from the calling matrix.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Subtract(matrices).
*/
func (m Matrix2) FSubtract(matrices ...*Matrix2) *Matrix2 {
	for _, n := range matrices {
		m[0] -= n[0]
		m[1] -= n[1]

		m[2] -= n[2]
		m[3] -= n[3]
	}

	return &m
}

/*
MultiplyScalar multiplies the calling matrix by a the given scalar.

The calling matrix itself is modified and overwritten.
If a functional operation is needed, use m.FMultiplyScalar(k).
*/
func (m *Matrix2) MultiplyScalar(k float64) {
	m[0] *= k
	m[1] *= k

	m[2] *= k
	m[3] *= k
}

/*
FMultiplyScalar multiplies the calling matrix by a the given scalar.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.MultiplyScalar(k).
*/
func (m Matrix2) FMultiplyScalar(k float64) *Matrix2 {
	m[0] *= k
	m[1] *= k

	m[2] *= k
	m[3] *= k

	return &m
}

/*
Multiply multiplies the calling matrix by the given matrices.

The calling matrix itself is modified and overwritten.
If a functional operation is needed, use m.FMultiply(matrices).
*/
func (m *Matrix2) Multiply(matrices ...*Matrix2) {
	for _, n := range matrices {
		m[0], m[1], m[2], m[3] =
			m[0]*n[0]+m[1]*n[2], m[0]*n[1]+m[1]*n[3],
			m[2]*n[0]+m[3]*n[2], m[2]*n[1]+m[3]*n[3]
	}
}

/*
FMultiply multiplies the calling matrix by the given matrices.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Multiply(matrices).
*/
func (m Matrix2) FMultiply(matrices ...*Matrix2) *Matrix2 {
	for _, n := range matrices {
		m[0], m[1], m[2], m[3] =
			m[0]*n[0]+m[1]*n[2], m[0]*n[1]+m[1]*n[3],
			m[2]*n[0]+m[3]*n[2], m[2]*n[1]+m[3]*n[3]
	}

	return &m
}

/*
Invert inverts the calling matrix, does nothing when the calling matrix is not invertible.

The calling matrix itself is modified and overwritten, unless it's not invertible.
If a functional operation is needed, use m.FInvert(matrices).
*/
func (m *Matrix2) Invert() {
	k := m[0]*m[3] - m[1]*m[2]

	if k == 0 {
		return
	}

	k = 1 / k
	m[0], m[1], m[2], m[3] =
		k*m[3], -k*m[1],
		-k*m[2], k*m[0]
}

/*
FInvert inverts the calling matrix, returns nil when the calling matrix is not invertible.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Invert(matrices).
*/
func (m Matrix2) FInvert() *Matrix2 {
	k := m[0]*m[3] - m[1]*m[2]

	if k == 0 {
		return nil
	}

	k = 1 / k
	m[0], m[1], m[2], m[3] =
		k*m[3], -k*m[1],
		-k*m[2], k*m[0]

	return &m
}

/*
Transpose transposes the calling matrix, switching its rows and columns.

The calling matrix itself is modified and overwritten.
If an in-place operation is needed, use m.Transpose(matrices).
*/
func (m *Matrix2) Transpose() {
	m[1], m[2] = m[2], m[1]
}

/*
FTranspose transposes the calling matrix, switching its rows and columns.

The calling matrix itself is not modified, a copy is created instead.
If an in-place operation is needed, use m.Transpose(matrices).
*/
func (m Matrix2) FTranspose() *Matrix2 {
	m[1], m[2] = m[2], m[1]
	return &m
}
