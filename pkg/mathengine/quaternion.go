package mathengine

import (
	"fmt"
	"math"
)

/*
Quaternion represents a 4-dimensional imaginary number.
Quaternions are used for rotating in 3-dimensional space along multiple axes.
*/
type Quaternion struct {
	/*
		S contains the Quaternion's scalar value.
	*/
	S float64
	/*
		V contains the Quaternion's imaginary vector.
	*/
	V Vector3
}

func (q Quaternion) String() string {
	return fmt.Sprintf("[%v, %v %v %v]", q.S, q.V.X, q.V.Y, q.V.Z)
}

/*
Add adds the given quaternions to the calling quaternion.

The calling quaternion itself is modified and overwritten.
If a functional operation is needed, use q.FAdd(quaternions).
*/
func (q *Quaternion) Add(quaternions ...*Quaternion) {
	for _, p := range quaternions {
		q.S += p.S
		q.V.X += p.V.X
		q.V.Y += p.V.Y
		q.V.Z += p.V.Z
	}
}

/*
FAdd adds the given quaternions to the calling quaternion.

The calling quaternion itself is not modified, a copy is created instead.
If an in-place operation is needed, use q.Add(quaternions).
*/
func (q Quaternion) FAdd(quaternions ...*Quaternion) *Quaternion {
	for _, p := range quaternions {
		q.S += p.S
		q.V.X += p.V.X
		q.V.Y += p.V.Y
		q.V.Z += p.V.Z
	}

	return &q
}

/*
Subtract subtracts the given quaternions from the calling quaternion.

The calling quaternion itself is modified and overwritten.
If a functional operation is needed, use q.FSubtract(quaternions).
*/
func (q *Quaternion) Subtract(quaternions ...*Quaternion) {
	for _, p := range quaternions {
		q.S -= p.S
		q.V.X -= p.V.X
		q.V.Y -= p.V.Y
		q.V.Z -= p.V.Z
	}
}

/*
FSubtract subtracts the given quaternions from the calling quaternion.

The calling quaternion itself is not modified, a copy is created instead.
If an in-place operation is needed, use q.Subtract(quaternions).
*/
func (q Quaternion) FSubtract(quaternions ...*Quaternion) *Quaternion {
	for _, p := range quaternions {
		q.S -= p.S
		q.V.X -= p.V.X
		q.V.Y -= p.V.Y
		q.V.Z -= p.V.Z
	}

	return &q
}

/*
MultiplyScalar multiplies the calling quaternion with the given scalar.

The calling quaternion itself is modified, and overwritten.
If a functional operation is needed, use q.FMultiplyScalar(quaternions).
*/
func (q *Quaternion) MultiplyScalar(k float64) {
	q.S *= k
	q.V.X *= k
	q.V.Y *= k
	q.V.Z *= k
}

/*
FMultiplyScalar multiplies the calling quaternion with the given scalar.

The calling quaternion itself is not modified, a copy is created instead.
If an in-place operation is needed, use q.MultiplyScalar(quaternions).
*/
func (q Quaternion) FMultiplyScalar(k float64) *Quaternion {
	q.S *= k
	q.V.X *= k
	q.V.Y *= k
	q.V.Z *= k

	return &q
}

/*
Multiply multiplies the given quaternions with the calling quaternion.

The calling quaternion itself is modified, and overwritten.
If a functional operation is needed, use q.FMultiply(quaternions).
*/
func (q *Quaternion) Multiply(quaternions ...*Quaternion) {
	for _, p := range quaternions {
		q.S, q.V.X, q.V.Y, q.V.Z =
			q.S*p.S-q.V.X*p.V.X-q.V.Y*p.V.Y-q.V.Z*p.V.Z,
			q.S*p.V.X+p.S*q.V.X+q.V.Y*p.V.Z-q.V.Z*p.V.Y,
			q.S*p.V.Y+p.S*q.V.Y+q.V.Z*p.V.X-q.V.X*p.V.Z,
			q.S*p.V.Z+p.S*q.V.Z+q.V.X*p.V.Y-q.V.Y*p.V.X
	}
}

/*
FMultiply multiplies the given quaternions with the calling quaternion.

The calling quaternion itself is not modified, a copy is created instead.
If an in-place operation is needed, use q.Multiply(quaternions).
*/
func (q Quaternion) FMultiply(quaternions ...*Quaternion) *Quaternion {
	for _, p := range quaternions {
		q.S, q.V.X, q.V.Y, q.V.Z =
			q.S*p.S-q.V.X*p.V.X-q.V.Y*p.V.Y-q.V.Z*p.V.Z,
			q.S*p.V.X+p.S*q.V.X+q.V.Y*p.V.Z-q.V.Z*p.V.Y,
			q.S*p.V.Y+p.S*q.V.Y+q.V.Z*p.V.X-q.V.X*p.V.Z,
			q.S*p.V.Z+p.S*q.V.Z+q.V.X*p.V.Y-q.V.Y*p.V.X
	}

	return &q
}

/*
Square squares the calling quaternion.

The calling quaternion itself is modified, and overwritten.
If a functional operation is needed, use q.FSquare().
*/
func (q *Quaternion) Square() {
	q.S, q.V.X, q.V.Y, q.V.Z =
		q.S*q.S-q.V.X*q.V.X-q.V.Y*q.V.Y-q.V.Z*q.V.Z,
		2*q.S*q.V.X,
		2*q.S*q.V.Y,
		2*q.S*q.V.Z
}

/*
FSquare squares the calling quaternion.

The calling quaternion itself is not modified, a copy is created instead.
If an in-place operation is needed, use q.Square().
*/
func (q Quaternion) FSquare(quaternions ...*Quaternion) *Quaternion {
	q.S, q.V.X, q.V.Y, q.V.Z =
		q.S*q.S-q.V.X*q.V.X-q.V.Y*q.V.Y-q.V.Z*q.V.Z,
		2*q.S*q.V.X,
		2*q.S*q.V.Y,
		2*q.S*q.V.Z

	return &q
}

/*
NormSquared calculates the squared norm of the calling quaternion.
*/
func (q Quaternion) NormSquared() float64 {
	return q.S*q.S + q.V.X*q.V.X + q.V.Y*q.V.Y + q.V.Z*q.V.Z
}

/*
Norm calculates the norm of the calling quaternion.
*/
func (q Quaternion) Norm() float64 {
	return math.Sqrt(q.S*q.S + q.V.X*q.V.X + q.V.Y*q.V.Y + q.V.Z*q.V.Z)
}

/*
Normalize normalizes the calling quaternion.

The calling quaternion itself is modified, and overwritten.
If a functional operation is needed, use q.FNormalize().
*/
func (q *Quaternion) Normalize() {
	k := 1 / math.Sqrt(q.S*q.S+q.V.X*q.V.X+q.V.Y*q.V.Y+q.V.Z*q.V.Z)
	q.S *= k
	q.V.X *= k
	q.V.Y *= k
	q.V.Z *= k
}

/*
FNormalize normalizes the calling quaternion.

The calling quaternion itself is not modified, a copy is created instead.
If an in-place operation is needed, use q.Normalize().
*/
func (q Quaternion) FNormalize() *Quaternion {
	k := 1 / math.Sqrt(q.S*q.S+q.V.X*q.V.X+q.V.Y*q.V.Y+q.V.Z*q.V.Z)
	q.S *= k
	q.V.X *= k
	q.V.Y *= k
	q.V.Z *= k

	return &q
}

/*
Conjugate calculates the conjugate of the calling quaternion which inverts the quaternions vector.

The calling quaternion itself is modified, and overwritten.
If a functional operation is needed, use q.FConjugate().
*/
func (q *Quaternion) Conjugate() {
	q.V.X *= -1
	q.V.Y *= -1
	q.V.Z *= -1
}

/*
FConjugate calculates the conjugate of the calling quaternion which inverts the quaternions vector.

The calling quaternion itself is not modified, a copy is created instead.
If an in-place operation is needed, use q.FConjugate().
*/
func (q Quaternion) FConjugate() *Quaternion {
	q.V.X *= -1
	q.V.Y *= -1
	q.V.Z *= -1

	return &q
}

/*
Invert inverts the calling quaternion.

The calling quaternion itself is modified, and overwritten.
If a functional operation is needed, use q.FInvert().
*/
func (q *Quaternion) Invert() {
	k := 1 / (q.S*q.S + q.V.X*q.V.X + q.V.Y*q.V.Y + q.V.Z*q.V.Z)
	q.S *= k
	q.V.X *= k
	q.V.Y *= k
	q.V.Z *= k
}

/*
FInvert inverts the calling quaternion.

The calling quaternion itself is not modified, a copy is created instead.
If an in-place operation is needed, use q.Invert().
*/
func (q Quaternion) FInvert() *Quaternion {
	k := 1 / (q.S*q.S + q.V.X*q.V.X + q.V.Y*q.V.Y + q.V.Z*q.V.Z)
	q.S *= k
	q.V.X *= k
	q.V.Y *= k
	q.V.Z *= k

	return &q
}
