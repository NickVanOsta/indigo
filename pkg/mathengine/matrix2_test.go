package mathengine

import (
	"fmt"
	"testing"
)

// === EXAMPLES ===

func ExampleIdentityMatrix2() {
	fmt.Println(IdentityMatrix2())
	//Output:
	// [[1 0] [0 1]]
}

func ExampleMatrix2_Add() {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	n := Matrix2{
		1, 2,
		1, 2,
	}
	p := Matrix2{
		3, 2,
		3, 2,
	}

	m.Add(&n, &p)
	fmt.Println(m)
	//Output:
	// [[5 5] [6 6]]
}

func ExampleMatrix2_FAdd() {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	n := Matrix2{
		1, 2,
		1, 2,
	}
	p := Matrix2{
		3, 2,
		3, 2,
	}

	fmt.Println(m.FAdd(&n, &p))
	//Output:
	// [[5 5] [6 6]]
}

func ExampleMatrix2_Subtract() {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	n := Matrix2{
		1, 2,
		1, 2,
	}
	p := Matrix2{
		3, 2,
		3, 2,
	}

	m.Subtract(&n, &p)
	fmt.Println(m)
	//Output:
	// [[-3 -3] [-2 -2]]
}

func ExampleMatrix2_FSubtract() {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	n := Matrix2{
		1, 2,
		1, 2,
	}
	p := Matrix2{
		3, 2,
		3, 2,
	}

	fmt.Println(m.FSubtract(&n, &p))
	//Output:
	// [[-3 -3] [-2 -2]]
}

func ExampleMatrix2_MultiplyScalar() {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	k := 2.

	m.MultiplyScalar(k)
	fmt.Println(m)
	//Output:
	// [[2 2] [4 4]]
}

func ExampleMatrix2_FMultiplyScalar() {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	k := 2.

	fmt.Println(m.FMultiplyScalar(k))
	//Output:
	// [[2 2] [4 4]]
}

func ExampleMatrix2_Multiply() {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	n := Matrix2{
		1, 2,
		1, 2,
	}
	p := Matrix2{
		3, 2,
		3, 2,
	}

	m.Multiply(&n, &p)
	fmt.Println(m)
	//Output:
	// [[18 12] [36 24]]
}

func ExampleMatrix2_FMultiply() {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	n := Matrix2{
		1, 2,
		1, 2,
	}
	p := Matrix2{
		3, 2,
		3, 2,
	}

	fmt.Println(m.FMultiply(&n, &p))
	//Output:
	// [[18 12] [36 24]]
}

func ExampleMatrix2_Invert_invertible() {
	m := Matrix2{
		2, 1,
		3, 2,
	}

	m.Invert()
	fmt.Println(m)
	//Output:
	// [[2 -1] [-3 2]]
}

func ExampleMatrix2_Invert_uninvertible() {
	m := Matrix2{
		1, 1,
		2, 2,
	}

	m.Invert()
	fmt.Println(m)
	//Output:
	// [[1 1] [2 2]]
}

func ExampleMatrix2_FInvert_invertible() {
	m := Matrix2{
		2, 1,
		3, 2,
	}

	fmt.Println(m.FInvert())
	//Output:
	// [[2 -1] [-3 2]]
}

func ExampleMatrix2_FInvert_uninvertible() {
	m := Matrix2{
		1, 1,
		2, 2,
	}

	fmt.Println(m.FInvert())
	//Output:
	// <nil>
}

func ExampleMatrix2_Transpose() {
	m := Matrix2{
		1, 1,
		2, 2,
	}

	m.Transpose()
	fmt.Println(m)
	//Output:
	// [[1 2] [1 2]]
}

func ExampleMatrix2_FTranspose() {
	m := Matrix2{
		1, 1,
		2, 2,
	}

	fmt.Println(m.FTranspose())
	//Output:
	// [[1 2] [1 2]]
}

// === TESTS ===

func TestIdentityMatrix2(t *testing.T) {
	t.Run("Creates the 2x2 identity matrix correctly", func(t *testing.T) {
		w := Matrix2{1, 0, 0, 1}
		if i := *IdentityMatrix2(); i != w {
			t.Errorf("Expected %v, but got %v.\n", w, i)
		}
	})
}

func TestMatrix2_Add(t *testing.T) {
	n := Matrix2{
		1, 2,
		1, 2,
	}
	i := Matrix2{
		1, 0,
		0, 1,
	}

	t.Run("Adds 2 matrices correctly", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}
		w := Matrix2{
			2, 3,
			3, 4,
		}

		if m.Add(&n); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Adds 3 matrices correctly", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}
		w := Matrix2{
			3, 3,
			3, 5,
		}

		if m.Add(&n, &i); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})
}

func TestMatrix2_FAdd(t *testing.T) {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	n := Matrix2{
		1, 2,
		1, 2,
	}
	i := Matrix2{
		1, 0,
		0, 1,
	}

	t.Run("Adds 2 matrices correctly", func(t *testing.T) {
		w := Matrix2{
			2, 3,
			3, 4,
		}

		if y := *m.FAdd(&n); y != w {
			t.Errorf("Expected %v, but got %v.\n", w, y)
		}
	})

	t.Run("Adds 3 matrices correctly", func(t *testing.T) {
		w := Matrix2{
			3, 3,
			3, 5,
		}

		if y := *m.FAdd(&n, &i); y != w {
			t.Errorf("Expected %v, but got %v.\n", w, y)
		}
	})

	t.Run("Creates a copy when no matrices are given", func(t *testing.T) {
		y := *m.FAdd()
		if y != m {
			t.Errorf("Expected %v, but got %v.\n", m, y)
		}
		if &y == &m {
			t.Errorf("Expected not %v, but got %v.\n", &m, &y)
		}
	})
}

func TestMatrix2_Subtract(t *testing.T) {
	n := Matrix2{
		1, 2,
		1, 2,
	}
	i := Matrix2{
		1, 0,
		0, 1,
	}

	t.Run("Subtracts 2 matrices correctly", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}
		w := Matrix2{
			0, -1,
			1, 0,
		}

		if m.Subtract(&n); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Subtracts 3 matrices correctly", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}
		w := Matrix2{
			-1, -1,
			1, -1,
		}

		if m.Subtract(&n, &i); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})
}

func TestMatrix2_FSubtract(t *testing.T) {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	n := Matrix2{
		1, 2,
		1, 2,
	}
	i := Matrix2{
		1, 0,
		0, 1,
	}

	t.Run("Subtracts 2 matrices correctly", func(t *testing.T) {
		w := Matrix2{
			0, -1,
			1, 0,
		}

		if y := *m.FSubtract(&n); y != w {
			t.Errorf("Expected %v, but got %v.\n", w, y)
		}
	})

	t.Run("Subtracts 3 matrices correctly", func(t *testing.T) {
		w := Matrix2{
			-1, -1,
			1, -1,
		}

		if y := *m.FSubtract(&n, &i); y != w {
			t.Errorf("Expected %v, but got %v.\n", w, y)
		}
	})

	t.Run("Creates a copy when no matrices are given", func(t *testing.T) {
		y := *m.FSubtract()
		if y != m {
			t.Errorf("Expected %v, but got %v.\n", m, y)
		}
		if &y == &m {
			t.Errorf("Expected not %v, but got %v.\n", &m, &y)
		}
	})
}

func TestMatrix2_MultiplyScalar(t *testing.T) {
	k := 5.

	t.Run("Scales the matrix with a factor >1 correctly", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}
		w := Matrix2{
			5, 5,
			10, 10,
		}

		if m.MultiplyScalar(k); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Scales the matrix with a factor <1 correctly", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}
		w := Matrix2{
			.2, .2,
			.4, .4,
		}

		m.MultiplyScalar(1 / k)
		for i, x := range m {
			if x-w[i] > 0.0001 {
				t.Errorf("Expected %v, but got %v.\n", w, m)
			}
		}
	})
}

func TestMatrix2_FMultiplyScalar(t *testing.T) {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	k := 5.

	t.Run("Scales the matrix with a factor >1 correctly", func(t *testing.T) {
		w := Matrix2{
			5, 5,
			10, 10,
		}

		if n := *m.FMultiplyScalar(k); n != w {
			t.Errorf("Expected %v, but got %v.\n", w, n)
		}
	})

	t.Run("Scales the matrix with a factor <1 correctly", func(t *testing.T) {
		w := Matrix2{
			.2, .2,
			.4, .4,
		}

		n := *m.FMultiplyScalar(1 / k)
		for i, x := range n {
			if x-w[i] > 0.0001 {
				t.Errorf("Expected %v, but got %v.\n", w, n)
			}
		}
	})
}

func TestMatrix2_Multiply(t *testing.T) {
	n := Matrix2{
		1, 2,
		1, 2,
	}
	i := Matrix2{
		1, 0,
		0, 1,
	}

	t.Run("Multiplies 2 matrices correctly", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}
		w := Matrix2{
			2, 4,
			4, 8,
		}

		if m.Multiply(&n); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Multiplies 3 matrices correctly", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}
		w := Matrix2{
			2, 4,
			4, 8,
		}

		if m.Multiply(&n, &i); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})
}

func TestMatrix2_FMultiply(t *testing.T) {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	n := Matrix2{
		1, 2,
		1, 2,
	}
	i := Matrix2{
		1, 0,
		0, 1,
	}

	t.Run("Multiplies 2 matrices correctly", func(t *testing.T) {
		w := Matrix2{
			2, 4,
			4, 8,
		}

		if n := *m.FMultiply(&n); n != w {
			t.Errorf("Expected %v, but got %v.\n", w, n)
		}
	})

	t.Run("Multiplies 3 matrices correctly", func(t *testing.T) {
		w := Matrix2{
			2, 4,
			4, 8,
		}

		if n := *m.FMultiply(&n, &i); n != w {
			t.Errorf("Expected %v, but got %v.\n", w, n)
		}
	})

	t.Run("Creates a copy when no matrices are given", func(t *testing.T) {
		y := *m.FMultiply()
		if y != m {
			t.Errorf("Expected %v, but got %v.\n", m, y)
		}
		if &y == &m {
			t.Errorf("Expected not %v, but got %v.\n", &m, &y)
		}
	})
}

func TestMatrix2_Invert(t *testing.T) {
	t.Run("Inverts an invertible matrix correctly", func(t *testing.T) {
		m := Matrix2{
			2, 1,
			3, 2,
		}
		w := Matrix2{
			2, -1,
			-3, 2,
		}

		if m.Invert(); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})

	t.Run("Returns nil when the matrix is not invertible", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}
		w := Matrix2{
			1, 1,
			2, 2,
		}

		if m.Invert(); m != w {
			t.Errorf("Expected %v, but got %v.\n", w, m)
		}
	})
}

func TestMatrix2_FInvert(t *testing.T) {
	t.Run("Inverts an invertible matrix correctly", func(t *testing.T) {
		m := Matrix2{
			2, 1,
			3, 2,
		}
		w := Matrix2{
			2, -1,
			-3, 2,
		}

		if n := *m.FInvert(); n != w {
			t.Errorf("Expected %v, but got %v.\n", w, n)
		}
	})

	t.Run("Returns nil when the matrix is not invertible", func(t *testing.T) {
		m := Matrix2{
			1, 1,
			2, 2,
		}

		if n := m.FInvert(); n != nil {
			t.Errorf("Expected %v, but got %v.\n", nil, n)
		}
	})
}

func TestMatrix2_Transpose(t *testing.T) {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	w := Matrix2{
		1, 2,
		1, 2,
	}

	if m.Transpose(); m != w {
		t.Errorf("Expected %v, but got %v.\n", w, m)
	}
}

func TestMatrix2_FTranspose(t *testing.T) {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	w := Matrix2{
		1, 2,
		1, 2,
	}

	if n := *m.FTranspose(); n != w {
		t.Errorf("Expected %v, but got %v.\n", w, n)
	}
}

// === BENCHMARKS ===

func BenchmarkIdentityMatrix2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		IdentityMatrix2()
	}
}

func BenchmarkMatrix2_Add(b *testing.B) {
	m := Matrix2{1, 1, 2, 2}

	b.Run("Add 2 matrices", func(b *testing.B) {
		n := Matrix2{2, 2, 4, 4}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Add(&n)
		}
	})

	b.Run("Add 8 matrices", func(b *testing.B) {
		n := []*Matrix2{
			{2, 2, 4, 4},
			{3, 3, 6, 6},
			{4, 4, 8, 8},
			{5, 5, 10, 10},
			{6, 6, 12, 12},
			{7, 7, 14, 14},
			{8, 8, 16, 16},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Add(n...)
		}
	})

	b.Run("Add no matrices", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Add()
		}
	})
}

func BenchmarkMatrix2_FAdd(b *testing.B) {
	m := Matrix2{1, 1, 2, 2}

	b.Run("Add 2 matrices", func(b *testing.B) {
		n := Matrix2{2, 2, 4, 4}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FAdd(&n)
		}
	})

	b.Run("Add 8 matrices", func(b *testing.B) {
		n := []*Matrix2{
			{2, 2, 4, 4},
			{3, 3, 6, 6},
			{4, 4, 8, 8},
			{5, 5, 10, 10},
			{6, 6, 12, 12},
			{7, 7, 14, 14},
			{8, 8, 16, 16},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FAdd(n...)
		}
	})

	b.Run("Copy a matrix", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FAdd()
		}
	})
}

func BenchmarkMatrix2_Subtract(b *testing.B) {
	m := Matrix2{1, 1, 2, 2}

	b.Run("Subtract 2 matrices", func(b *testing.B) {
		n := Matrix2{2, 2, 4, 4}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Subtract(&n)
		}
	})

	b.Run("Subtract 8 matrices", func(b *testing.B) {
		n := []*Matrix2{
			{2, 2, 4, 4},
			{3, 3, 6, 6},
			{4, 4, 8, 8},
			{5, 5, 10, 10},
			{6, 6, 12, 12},
			{7, 7, 14, 14},
			{8, 8, 16, 16},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Subtract(n...)
		}
	})

	b.Run("Subtract no matrices", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Subtract()
		}
	})
}

func BenchmarkMatrix2_FSubtract(b *testing.B) {
	m := Matrix2{1, 1, 2, 2}

	b.Run("Subtract 2 matrices", func(b *testing.B) {
		n := Matrix2{2, 2, 4, 4}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FSubtract(&n)
		}
	})

	b.Run("Subtract 8 matrices", func(b *testing.B) {
		n := []*Matrix2{
			{2, 2, 4, 4},
			{3, 3, 6, 6},
			{4, 4, 8, 8},
			{5, 5, 10, 10},
			{6, 6, 12, 12},
			{7, 7, 14, 14},
			{8, 8, 16, 16},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FSubtract(n...)
		}
	})

	b.Run("Copy a matrix", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FSubtract()
		}
	})
}

func BenchmarkMatrix2_MultiplyScalar(b *testing.B) {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	k := 3.141592

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.MultiplyScalar(k)
	}
}

func BenchmarkMatrix2_FMultiplyScalar(b *testing.B) {
	m := Matrix2{
		1, 1,
		2, 2,
	}
	k := 3.141592

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.FMultiplyScalar(k)
	}
}

func BenchmarkMatrix2_Multiply(b *testing.B) {
	m := Matrix2{1, 1, 2, 2}

	b.Run("Multiply 2 matrices", func(b *testing.B) {
		n := Matrix2{2, 2, 4, 4}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Multiply(&n)
		}
	})

	b.Run("Multiply 8 matrices", func(b *testing.B) {
		n := []*Matrix2{
			{2, 2, 4, 4},
			{3, 3, 6, 6},
			{4, 4, 8, 8},
			{5, 5, 10, 10},
			{6, 6, 12, 12},
			{7, 7, 14, 14},
			{8, 8, 16, 16},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Multiply(n...)
		}
	})

	b.Run("Multiply no matrices", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Multiply()
		}
	})
}

func BenchmarkMatrix2_FMultiply(b *testing.B) {
	m := Matrix2{1, 1, 2, 2}

	b.Run("Multiply 2 matrices", func(b *testing.B) {
		n := Matrix2{2, 2, 4, 4}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FMultiply(&n)
		}
	})

	b.Run("Multiply 8 matrices", func(b *testing.B) {
		n := []*Matrix2{
			{2, 2, 4, 4},
			{3, 3, 6, 6},
			{4, 4, 8, 8},
			{5, 5, 10, 10},
			{6, 6, 12, 12},
			{7, 7, 14, 14},
			{8, 8, 16, 16},
		}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FMultiply(n...)
		}
	})

	b.Run("Copy a matrix", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FMultiply()
		}
	})
}

func BenchmarkMatrix2_Invert(b *testing.B) {
	b.Run("Invertible matrix", func(b *testing.B) {
		m := Matrix2{
			2, 1,
			3, 2,
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Invert()
		}
	})

	b.Run("Uninvertible matrix", func(b *testing.B) {
		m := Matrix2{
			1, 1,
			2, 2,
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.Invert()
		}
	})
}

func BenchmarkMatrix2_FInvert(b *testing.B) {
	b.Run("Invertible matrix", func(b *testing.B) {
		m := Matrix2{
			2, 1,
			3, 2,
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FInvert()
		}
	})

	b.Run("Uninvertible matrix", func(b *testing.B) {
		m := Matrix2{
			1, 1,
			2, 2,
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			m.FInvert()
		}
	})
}

func BenchmarkMatrix2_Transpose(b *testing.B) {
	m := Matrix2{
		1, 1,
		2, 2,
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.Transpose()
	}
}

func BenchmarkMatrix2_FTranspose(b *testing.B) {
	m := Matrix2{
		1, 1,
		2, 2,
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		m.FTranspose()
	}
}
