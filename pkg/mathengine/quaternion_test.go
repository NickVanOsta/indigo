package mathengine

import (
	"fmt"
	"testing"
)

// === EXAMPLES ===

func ExampleQuaternion_Add() {
	q := Quaternion{1, Vector3{2, 3, 4}}
	p := Quaternion{1, Vector3{4, 9, 16}}
	r := Quaternion{1, Vector3{8, 27, 64}}

	q.Add(&p, &r)
	fmt.Println(q)
	//Output:
	// [3, 14 39 84]
}

func ExampleQuaternion_FAdd() {
	q := Quaternion{1, Vector3{2, 3, 4}}
	p := Quaternion{1, Vector3{4, 9, 16}}
	r := Quaternion{1, Vector3{8, 27, 64}}

	fmt.Println(q.FAdd(&p, &r))
	//Output:
	// [3, 14 39 84]
}

func ExampleQuaternion_Subtract() {
	q := Quaternion{1, Vector3{2, 3, 4}}
	p := Quaternion{1, Vector3{4, 9, 16}}
	r := Quaternion{1, Vector3{8, 27, 64}}

	q.Subtract(&p, &r)
	fmt.Println(q)
	//Output:
	// [-1, -10 -33 -76]
}

func ExampleQuaternion_FSubtract() {
	q := Quaternion{1, Vector3{2, 3, 4}}
	p := Quaternion{1, Vector3{4, 9, 16}}
	r := Quaternion{1, Vector3{8, 27, 64}}

	fmt.Println(q.FSubtract(&p, &r))
	//Output:
	// [-1, -10 -33 -76]
}

func ExampleQuaternion_MultiplyScalar() {
	q := Quaternion{1, Vector3{2, 3, 4}}
	k := 2.

	q.MultiplyScalar(k)
	fmt.Println(q)
	//Output:
	// [2, 4 6 8]
}

func ExampleQuaternion_FMultiplyScalar() {
	q := Quaternion{1, Vector3{2, 3, 4}}
	k := 2.

	fmt.Println(q.FMultiplyScalar(k))
	//Output:
	// [2, 4 6 8]
}

func ExampleQuaternion_Multiply() {
	q := Quaternion{1, Vector3{2, 3, 4}}
	p := Quaternion{1, Vector3{4, 9, 16}}
	r := Quaternion{1, Vector3{8, 27, 64}}

	q.Multiply(&p, &r)
	fmt.Println(q)
	//Output:
	// [-1798, -1724 -3594 -5728]
}

func ExampleQuaternion_FMultiply() {
	q := Quaternion{1, Vector3{2, 3, 4}}
	p := Quaternion{1, Vector3{4, 9, 16}}
	r := Quaternion{1, Vector3{8, 27, 64}}

	fmt.Println(q.FMultiply(&p, &r))
	//Output:
	// [-1798, -1724 -3594 -5728]
}

func ExampleQuaternion_Square() {
	q := Quaternion{1, Vector3{2, 3, 4}}

	q.Square()
	fmt.Println(q)
	//Output:
	// [-28, 4 6 8]
}

func ExampleQuaternion_FSquare() {
	q := Quaternion{1, Vector3{2, 3, 4}}

	fmt.Println(q.FSquare())
	//Output:
	// [-28, 4 6 8]
}

func ExampleQuaternion_NormSquared() {
	q := Quaternion{27, Vector3{36, 60, 100}}

	fmt.Println(q.NormSquared())
	//Output:
	// 15625
}

func ExampleQuaternion_Norm() {
	q := Quaternion{27, Vector3{36, 60, 100}}

	fmt.Println(q.Norm())
	//Output:
	// 125
}

func ExampleQuaternion_Normalize() {
	q := Quaternion{27, Vector3{36, 60, 100}}

	q.Normalize()
	fmt.Println(q)
	//Output:
	// [0.216, 0.28800000000000003 0.48 0.8]
}

func ExampleQuaternion_FNormalize() {
	q := Quaternion{27, Vector3{36, 60, 100}}

	fmt.Println(q.FNormalize())
	//Output:
	// [0.216, 0.28800000000000003 0.48 0.8]
}

func ExampleQuaternion_Conjugate() {
	q := Quaternion{1, Vector3{2, 3, 4}}

	q.Conjugate()
	fmt.Println(q)
	//Output:
	// [1, -2 -3 -4]
}

func ExampleQuaternion_FConjugate() {
	q := Quaternion{1, Vector3{2, 3, 4}}

	fmt.Println(q.FConjugate())
	//Output:
	// [1, -2 -3 -4]
}

func ExampleQuaternion_Invert() {
	q := Quaternion{1, Vector3{2, 3, 4}}

	q.Invert()
	fmt.Println(q)
	//Output:
	// [0.03333333333333333, 0.06666666666666667 0.1 0.13333333333333333]
}

func ExampleQuaternion_FInvert() {
	q := Quaternion{1, Vector3{2, 3, 4}}

	fmt.Println(q.FInvert())
	//Output:
	// [0.03333333333333333, 0.06666666666666667 0.1 0.13333333333333333]
}

// === TESTS ===

func TestQuaternion_Add(t *testing.T) {
	b := Quaternion{1, Vector3{4, 9, 16}}
	c := Quaternion{1, Vector3{8, 27, 64}}

	t.Run("Adds 2 quaternions correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{2, Vector3{6, 12, 20}}

		if a.Add(&b); a != w {
			t.Errorf("Expected %v, but got %v.\n", w, a)
		}
	})

	t.Run("Adds 3 quaternions correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{3, Vector3{14, 39, 84}}

		if a.Add(&b, &c); a != w {
			t.Errorf("Expected %v, but got %v.\n", w, a)
		}
	})
}

func TestQuaternion_FAdd(t *testing.T) {
	a := Quaternion{1, Vector3{2, 3, 4}}
	b := Quaternion{1, Vector3{4, 9, 16}}
	c := Quaternion{1, Vector3{8, 27, 64}}

	t.Run("Adds 2 quaternions correctly", func(t *testing.T) {
		w := Quaternion{2, Vector3{6, 12, 20}}

		if z := *a.FAdd(&b); z != w {
			t.Errorf("Expected %v, but got %v.\n", w, z)
		}
	})

	t.Run("Adds 3 quaternions correctly", func(t *testing.T) {
		w := Quaternion{3, Vector3{14, 39, 84}}

		if z := *a.FAdd(&b, &c); z != w {
			t.Errorf("Expected %v, but got %v.\n", w, z)
		}
	})

	t.Run("Creates a copy when no quaternions are given", func(t *testing.T) {
		z := *a.FAdd()
		if z != a {
			t.Errorf("Expected %v, but got %v.\n", a, z)
		}
		if &z == &a {
			t.Errorf("Expected not %v, but got %v.\n", &a, &z)
		}
	})
}

func TestQuaternion_Subtract(t *testing.T) {
	b := Quaternion{1, Vector3{4, 9, 16}}
	c := Quaternion{1, Vector3{8, 27, 64}}

	t.Run("Subtracts 2 quaternions correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{0, Vector3{-2, -6, -12}}

		if a.Subtract(&b); a != w {
			t.Errorf("Expected %v, but got %v.\n", w, a)
		}
	})

	t.Run("Subtracts 3 quaternions correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{-1, Vector3{-10, -33, -76}}

		if a.Subtract(&b, &c); a != w {
			t.Errorf("Expected %v, but got %v.\n", w, a)
		}
	})
}

func TestQuaternion_FSubtract(t *testing.T) {
	a := Quaternion{1, Vector3{2, 3, 4}}
	b := Quaternion{1, Vector3{4, 9, 16}}
	c := Quaternion{1, Vector3{8, 27, 64}}

	t.Run("Subtracts 2 quaternions correctly", func(t *testing.T) {
		w := Quaternion{0, Vector3{-2, -6, -12}}

		if z := *a.FSubtract(&b); z != w {
			t.Errorf("Expected %v, but got %v.\n", w, z)
		}
	})

	t.Run("Subtracts 3 quaternions correctly", func(t *testing.T) {
		w := Quaternion{-1, Vector3{-10, -33, -76}}

		if z := *a.FSubtract(&b, &c); z != w {
			t.Errorf("Expected %v, but got %v.\n", w, z)
		}
	})

	t.Run("Creates a copy when no quaternions are given", func(t *testing.T) {
		z := *a.FSubtract()
		if z != a {
			t.Errorf("Expected %v, but got %v.\n", a, z)
		}
		if &z == &a {
			t.Errorf("Expected not %v, but got %v.\n", &a, &z)
		}
	})
}

func TestQuaternion_MultiplyScalar(t *testing.T) {
	k := 5.

	t.Run("Multiplies the quaternion with a factor >1 correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{5, Vector3{10, 15, 20}}

		if a.MultiplyScalar(k); a != w {
			t.Errorf("Expected %v, but got %v.\n", w, a)
		}
	})

	t.Run("Multiplies the quaternion with a factor >1 correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{.2, Vector3{.4, .6, .8}}

		if a.MultiplyScalar(1 / k); a.S-w.S > .0001 || a.V.X-w.V.X > .0001 || a.V.Y-w.V.Y > .0001 || a.V.Z-w.V.Z > .0001 {
			t.Errorf("Expected %v, but got %v.\n", w, a)
		}
	})
}

func TestQuaternion_FMultiplyScalar(t *testing.T) {
	k := 5.

	t.Run("Multiplies the quaternion with a factor >1 correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{5, Vector3{10, 15, 20}}

		if z := *a.FMultiplyScalar(k); z != w {
			t.Errorf("Expected %v, but got %v.\n", w, z)
		}
	})

	t.Run("Multiplies the quaternion with a factor >1 correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{.2, Vector3{.4, .6, .8}}

		if z := *a.FMultiplyScalar(1 / k); z.S-w.S > .0001 || z.V.X-w.V.X > .0001 || z.V.Y-w.V.Y > .0001 || z.V.Z-w.V.Z > .0001 {
			t.Errorf("Expected %v, but got %v.\n", w, z)
		}
	})
}

func TestQuaternion_Multiply(t *testing.T) {
	b := Quaternion{1, Vector3{4, 9, 16}}
	c := Quaternion{1, Vector3{8, 27, 64}}

	t.Run("Multiplies 2 quaternions correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{-98, Vector3{18, -4, 26}}

		if a.Multiply(&b); a != w {
			t.Errorf("Expected %v, but got %v.\n", w, a)
		}
	})

	t.Run("Multiplies 3 quaternions correctly", func(t *testing.T) {
		a := Quaternion{1, Vector3{2, 3, 4}}
		w := Quaternion{-1798, Vector3{-1724, -3594, -5728}}

		if a.Multiply(&b, &c); a != w {
			t.Errorf("Expected %v, but got %v.\n", w, a)
		}
	})
}

func TestQuaternion_FMultiply(t *testing.T) {
	a := Quaternion{1, Vector3{2, 3, 4}}
	b := Quaternion{1, Vector3{4, 9, 16}}
	c := Quaternion{1, Vector3{8, 27, 64}}

	t.Run("Multiplies 2 quaternions correctly", func(t *testing.T) {
		w := Quaternion{-98, Vector3{18, -4, 26}}

		if z := *a.FMultiply(&b); z != w {
			t.Errorf("Expected %v, but got %v.\n", w, z)
		}
	})

	t.Run("Multiplies 3 quaternions correctly", func(t *testing.T) {
		w := Quaternion{-1798, Vector3{-1724, -3594, -5728}}

		if z := *a.FMultiply(&b, &c); z != w {
			t.Errorf("Expected %v, but got %v.\n", w, z)
		}
	})

	t.Run("Creates a copy when no quaternions are given", func(t *testing.T) {
		z := *a.FMultiply()
		if z != a {
			t.Errorf("Expected %v, but got %v.\n", a, z)
		}
		if &z == &a {
			t.Errorf("Expected not %v, but got %v.\n", &a, &z)
		}
	})
}

func TestQuaternion_Square(t *testing.T) {
	q := Quaternion{1, Vector3{2, 3, 4}}
	w := Quaternion{-28, Vector3{4, 6, 8}}

	if q.Square(); q != w {
		t.Errorf("Expected %v, but got %v.\n", w, q)
	}
}

func TestQuaternion_FSquare(t *testing.T) {
	q := Quaternion{1, Vector3{2, 3, 4}}
	w := Quaternion{-28, Vector3{4, 6, 8}}

	if z := *q.FSquare(); z != w {
		t.Errorf("Expected %v, but got %v.\n", w, z)
	}
}

func TestQuaternion_NormSquared(t *testing.T) {
	q := Quaternion{27, Vector3{36, 60, 100}}
	w := 15625.

	if n := q.NormSquared(); n != w {
		t.Errorf("Expected %v, but got %v.\n", w, n)
	}
}

func TestQuaternion_Norm(t *testing.T) {
	q := Quaternion{27, Vector3{36, 60, 100}}
	w := 125.

	if n := q.Norm(); n != w {
		t.Errorf("Expected %v, but got %v.\n", w, n)
	}
}

func TestQuaternion_Normalize(t *testing.T) {
	q := Quaternion{27, Vector3{36, 60, 100}}
	w := Quaternion{.216, Vector3{.288, .48, .8}}

	if q.Normalize(); q.S-w.S > .0001 || q.V.X-w.V.X > .0001 || q.V.Y-w.V.Y > .0001 || q.V.Z-w.V.Z > .0001 {
		t.Errorf("Expected %v, but got %v.\n", w, q)
	}
}

func TestQuaternion_FNormalize(t *testing.T) {
	q := Quaternion{27, Vector3{36, 60, 100}}
	w := Quaternion{.216, Vector3{.288, .48, .8}}

	if z := *q.FNormalize(); z.S-w.S > .0001 || z.V.X-w.V.X > .0001 || z.V.Y-w.V.Y > .0001 || z.V.Z-w.V.Z > .0001 {
		t.Errorf("Expected %v, but got %v.\n", w, z)
	}
}

func TestQuaternion_Conjugate(t *testing.T) {
	q := Quaternion{1, Vector3{2, 3, 4}}
	w := Quaternion{1, Vector3{-2, -3, -4}}

	if q.Conjugate(); q != w {
		t.Errorf("Expected %v, but got %v.\n", w, q)
	}
}

func TestQuaternion_FConjugate(t *testing.T) {
	q := Quaternion{1, Vector3{2, 3, 4}}
	w := Quaternion{1, Vector3{-2, -3, -4}}

	if z := *q.FConjugate(); z != w {
		t.Errorf("Expected %v, but got %v.\n", w, z)
	}
}

func TestQuaternion_Invert(t *testing.T) {
	q := Quaternion{1, Vector3{2, 3, 4}}
	w := Quaternion{1. / 30, Vector3{1. / 15, 1. / 10, 2. / 15}}

	if q.Invert(); q != w {
		t.Errorf("Expected %v, but got %v.\n", w, q)
	}
}

func TestQuaternion_FInvert(t *testing.T) {
	q := Quaternion{1, Vector3{2, 3, 4}}
	w := Quaternion{1. / 30, Vector3{1. / 15, 1. / 10, 2. / 15}}

	if z := *q.FInvert(); z != w {
		t.Errorf("Expected %v, but got %v.\n", w, z)
	}
}

// === BENCHMARKS ===

func BenchmarkQuaternion_Add(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.Run("Add 2 quaternions", func(b *testing.B) {
		p := &Quaternion{2, Vector3{4, 6, 8}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.Add(p)
		}
	})

	b.Run("Add 8 quaternions", func(b *testing.B) {
		p := []*Quaternion{
			{2, Vector3{4, 6, 8}},
			{3, Vector3{6, 9, 12}},
			{4, Vector3{8, 12, 16}},
			{5, Vector3{10, 15, 20}},
			{6, Vector3{12, 18, 24}},
			{7, Vector3{14, 21, 28}},
			{8, Vector3{16, 24, 32}},
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.Add(p...)
		}
	})

	b.Run("Add no quaternions", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.Add()
		}
	})
}

func BenchmarkQuaternion_FAdd(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.Run("Add 2 quaternions", func(b *testing.B) {
		p := &Quaternion{2, Vector3{4, 6, 8}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.FAdd(p)
		}
	})

	b.Run("Add 8 quaternions", func(b *testing.B) {
		p := []*Quaternion{
			{2, Vector3{4, 6, 8}},
			{3, Vector3{6, 9, 12}},
			{4, Vector3{8, 12, 16}},
			{5, Vector3{10, 15, 20}},
			{6, Vector3{12, 18, 24}},
			{7, Vector3{14, 21, 28}},
			{8, Vector3{16, 24, 32}},
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.FAdd(p...)
		}
	})

	b.Run("Copy a quaternion", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.FAdd()
		}
	})
}

func BenchmarkQuaternion_Subtract(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.Run("Subtract 2 quaternions", func(b *testing.B) {
		p := &Quaternion{2, Vector3{4, 6, 8}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.Subtract(p)
		}
	})

	b.Run("Subtract 8 quaternions", func(b *testing.B) {
		p := []*Quaternion{
			{2, Vector3{4, 6, 8}},
			{3, Vector3{6, 9, 12}},
			{4, Vector3{8, 12, 16}},
			{5, Vector3{10, 15, 20}},
			{6, Vector3{12, 18, 24}},
			{7, Vector3{14, 21, 28}},
			{8, Vector3{16, 24, 32}},
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.Subtract(p...)
		}
	})

	b.Run("Subtract no quaternions", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.Subtract()
		}
	})
}

func BenchmarkQuaternion_FSubtract(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.Run("Subtract 2 quaternions", func(b *testing.B) {
		p := &Quaternion{2, Vector3{4, 6, 8}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.FSubtract(p)
		}
	})

	b.Run("Subtract 8 quaternions", func(b *testing.B) {
		p := []*Quaternion{
			{2, Vector3{4, 6, 8}},
			{3, Vector3{6, 9, 12}},
			{4, Vector3{8, 12, 16}},
			{5, Vector3{10, 15, 20}},
			{6, Vector3{12, 18, 24}},
			{7, Vector3{14, 21, 28}},
			{8, Vector3{16, 24, 32}},
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.FSubtract(p...)
		}
	})

	b.Run("Copy a quaternion", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.FSubtract()
		}
	})
}

func BenchmarkQuaternion_MultiplyScalar(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}
	k := 3.14159265359

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.MultiplyScalar(k)
	}
}

func BenchmarkQuaternion_FMultiplyScalar(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}
	k := 3.14159265359

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.FMultiplyScalar(k)
	}
}

func BenchmarkQuaternion_Multiply(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.Run("Multiply 2 quaternions", func(b *testing.B) {
		p := &Quaternion{2, Vector3{4, 6, 8}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.Multiply(p)
		}
	})

	b.Run("Multiply 8 quaternions", func(b *testing.B) {
		p := []*Quaternion{
			{2, Vector3{4, 6, 8}},
			{3, Vector3{6, 9, 12}},
			{4, Vector3{8, 12, 16}},
			{5, Vector3{10, 15, 20}},
			{6, Vector3{12, 18, 24}},
			{7, Vector3{14, 21, 28}},
			{8, Vector3{16, 24, 32}},
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.Multiply(p...)
		}
	})

	b.Run("Multiply no quaternions", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.Multiply()
		}
	})
}

func BenchmarkQuaternion_FMultiply(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.Run("Multiply 2 quaternions", func(b *testing.B) {
		p := &Quaternion{2, Vector3{4, 6, 8}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.FMultiply(p)
		}
	})

	b.Run("Multiply 8 quaternions", func(b *testing.B) {
		p := []*Quaternion{
			{2, Vector3{4, 6, 8}},
			{3, Vector3{6, 9, 12}},
			{4, Vector3{8, 12, 16}},
			{5, Vector3{10, 15, 20}},
			{6, Vector3{12, 18, 24}},
			{7, Vector3{14, 21, 28}},
			{8, Vector3{16, 24, 32}},
		}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.FMultiply(p...)
		}
	})

	b.Run("Copy a quaternion", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			q.FMultiply()
		}
	})
}

func BenchmarkQuaternion_Square(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.Square()
	}
}

func BenchmarkQuaternion_FSquare(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.FSquare()
	}
}

func BenchmarkQuaternion_NormSquared(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.NormSquared()
	}
}

func BenchmarkQuaternion_Norm(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.Norm()
	}
}

func BenchmarkQuaternion_Normalize(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.Normalize()
	}
}

func BenchmarkQuaternion_FNormalize(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.FNormalize()
	}
}

func BenchmarkQuaternion_Conjugate(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.Conjugate()
	}
}

func BenchmarkQuaternion_FConjugate(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.FConjugate()
	}
}

func BenchmarkQuaternion_Invert(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.Invert()
	}
}

func BenchmarkQuaternion_FInvert(b *testing.B) {
	q := &Quaternion{1, Vector3{2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		q.FInvert()
	}
}
