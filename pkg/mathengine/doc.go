/*
Package mathengine provides the mathematical core of the Indigo Game Engine.

The package contains the basics needed for the linear algebra calculations used by the game engine.
This includes 2D vectors, 3D vectors, 2x2 Matrices, 3x3 Matrices, and Quaternions.
*/
package mathengine
