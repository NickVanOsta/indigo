package mathengine

import (
	"fmt"
	"math"
)

/*
Vector2 represents a 2-dimensional point or direction.
*/
type Vector2 struct {
	X, Y float64
}

func (u Vector2) String() string {
	return fmt.Sprintf("[%v %v]", u.X, u.Y)
}

/*
ZeroVector2 creates the zero vector, a vector with a length of 0.
*/
func ZeroVector2() *Vector2 {
	return &Vector2{0, 0}
}

/*
UnitXVector2 creates the unit x vector, a vector with a length of 1 along the X-axis.
*/
func UnitXVector2() *Vector2 {
	return &Vector2{1, 0}
}

/*
UnitYVector2 creates the unit y vector, a vector with a length of 1 along the Y-axis.
*/
func UnitYVector2() *Vector2 {
	return &Vector2{0, 1}
}

/*
Add adds the given list of vectors from the calling vector.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FAdd().
*/
func (u *Vector2) Add(vectors ...*Vector2) {
	for _, v := range vectors {
		u.X += v.X
		u.Y += v.Y
	}
}

/*
FAdd adds the given list of vectors to the calling vector.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Add(vectors).
*/
func (u Vector2) FAdd(vectors ...*Vector2) *Vector2 {
	for _, v := range vectors {
		u.X += v.X
		u.Y += v.Y
	}

	return &u
}

/*
Subtract subtracts the given list of vectors from the calling vector.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FSubtract(vectors).
*/
func (u *Vector2) Subtract(vectors ...*Vector2) {
	for _, v := range vectors {
		u.X -= v.X
		u.Y -= v.Y
	}
}

/*
FSubtract subtracts the given list of vectors from the calling vector.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Subtract(vectors).
*/
func (u Vector2) FSubtract(vectors ...*Vector2) *Vector2 {
	for _, v := range vectors {
		u.X -= v.X
		u.Y -= v.Y
	}

	return &u
}

/*
Dot calculates the dot product of the calling vector and the given vector.
*/
func (u Vector2) Dot(v *Vector2) float64 {
	return u.X*v.X + u.Y*v.Y
}

/*
Scale scales the calling vector with a given factor k.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FScale(k).
*/
func (u *Vector2) Scale(k float64) {
	u.X *= k
	u.Y *= k
}

/*
FScale scales the calling vector with a given factor k.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Scale(k).
*/
func (u Vector2) FScale(k float64) *Vector2 {
	u.X *= k
	u.Y *= k

	return &u
}

/*
NormSquared calculates the squared length of the calling vector.
*/
func (u Vector2) NormSquared() float64 {
	return u.X*u.X + u.Y*u.Y
}

/*
Norm calculates the length of the calling vector.
*/
func (u Vector2) Norm() float64 {
	return math.Sqrt(u.X*u.X + u.Y*u.Y)
}

/*
Normalize normalizes the calling vector, returning a vector with a length equal to 1.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FNormalize().
*/
func (u *Vector2) Normalize() {
	k := 1 / math.Sqrt(u.X*u.X+u.Y*u.Y)
	u.X *= k
	u.Y *= k
}

/*
FNormalize normalizes the calling vector, returning a vector with a length equal to 1.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Normalize().
*/
func (u Vector2) FNormalize() *Vector2 {
	k := 1 / math.Sqrt(u.X*u.X+u.Y*u.Y)
	u.X *= k
	u.Y *= k

	return &u
}

/*
Rotate rotates the calling vector over angle radians.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FRotate(angle).
*/
func (u *Vector2) Rotate(angle float64) {
	cos, sin := math.Cos(angle), math.Sin(angle)
	u.X, u.Y = u.X*cos-u.Y*sin, u.X*sin+u.Y*cos
}

/*
FRotate rotates the calling vector over angle radians.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Rotate(angle).
*/
func (u Vector2) FRotate(angle float64) *Vector2 {
	cos, sin := math.Cos(angle), math.Sin(angle)
	u.X, u.Y = u.X*cos-u.Y*sin, u.X*sin+u.Y*cos

	return &u
}

/*
RotateAndTranslate rotates and translates the calling vector over angle radians and a given translation.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FRotateAndTranslate(angle).
*/
func (u *Vector2) RotateAndTranslate(angle float64, translation *Vector2) {
	cos, sin := math.Cos(angle), math.Sin(angle)
	u.X, u.Y = u.X*cos-u.Y*sin+translation.X, u.X*sin+u.Y*cos+translation.Y
}

/*
FRotateAndTranslate rotates and translates the calling vector over angle radians and a given translation.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.RotateAndTranslate(angle).
*/
func (u Vector2) FRotateAndTranslate(angle float64, translation *Vector2) *Vector2 {
	cos, sin := math.Cos(angle), math.Sin(angle)
	u.X, u.Y = u.X*cos-u.Y*sin+translation.X, u.X*sin+u.Y*cos+translation.Y

	return &u
}
