package mathengine

import (
	"fmt"
	"math"
)

/*
Vector3 represents a 3-dimensional point or direction.
*/
type Vector3 struct {
	X, Y, Z float64
}

func (u Vector3) String() string {
	return fmt.Sprintf("[%v %v %v]", u.X, u.Y, u.Z)
}

/*
ZeroVector3 creates the zero vector, a vector with a length of 0.
*/
func ZeroVector3() *Vector3 {
	return &Vector3{0, 0, 0}
}

/*
UnitXVector3 creates the unit x vector, a vector with a length of 1 along the X-axis.
*/
func UnitXVector3() *Vector3 {
	return &Vector3{1, 0, 0}
}

/*
UnitYVector3 creates the unit y vector, a vector with a length of 1 along the Y-axis.
*/
func UnitYVector3() *Vector3 {
	return &Vector3{0, 1, 0}
}

/*
UnitZVector3 creates the unit z vector, a vector with a length of 1 along the Z-axis.
*/
func UnitZVector3() *Vector3 {
	return &Vector3{0, 0, 1}
}

/*
Add adds the given list of vectors to the first given vector.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FAdd(vectors).
*/
func (u *Vector3) Add(vectors ...*Vector3) {
	for _, v := range vectors {
		u.X += v.X
		u.Y += v.Y
		u.Z += v.Z
	}
}

/*
FAdd adds the given list of vectors to the calling vector.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Add(vectors).
*/
func (u Vector3) FAdd(vectors ...*Vector3) *Vector3 {
	for _, v := range vectors {
		u.X += v.X
		u.Y += v.Y
		u.Z += v.Z
	}

	return &u
}

/*
Subtract subtracts the given list of vectors to the first given vector.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FSubtract(vectors).
*/
func (u *Vector3) Subtract(vectors ...*Vector3) {
	for _, v := range vectors {
		u.X -= v.X
		u.Y -= v.Y
		u.Z -= v.Z
	}
}

/*
FSubtract subtracts the given list of vectors from the calling vector.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Subtract(vectors).
*/
func (u Vector3) FSubtract(vectors ...*Vector3) *Vector3 {
	for _, v := range vectors {
		u.X -= v.X
		u.Y -= v.Y
		u.Z -= v.Z
	}

	return &u
}

/*
Dot calculates the dot product of the calling vector and the given vector.
*/
func (u Vector3) Dot(v *Vector3) float64 {
	return u.X*v.X + u.Y*v.Y + u.Z*v.Z
}

/*
Cross calculates the cross product of the calling vector and the given vectors.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FCross(vectors).
*/
func (u *Vector3) Cross(vectors ...*Vector3) {
	for _, v := range vectors {
		u.X, u.Y, u.Z = u.Y*v.Z-u.Z*v.Y, u.Z*v.X-u.X*v.Z, u.X*v.Y-u.Y*v.X
	}
}

/*
FCross calculates the cross product of the calling vector and the given vectors.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Cross(vectors).
*/
func (u Vector3) FCross(vectors ...*Vector3) *Vector3 {
	for _, v := range vectors {
		u.X, u.Y, u.Z = u.Y*v.Z-u.Z*v.Y, u.Z*v.X-u.X*v.Z, u.X*v.Y-u.Y*v.X
	}

	return &u
}

/*
Scale scales the calling vector with a given factor k.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FScale(k).
*/
func (u *Vector3) Scale(k float64) {
	u.X *= k
	u.Y *= k
	u.Z *= k
}

/*
FScale scales the calling vector with a given factor k.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Scale(k).
*/
func (u Vector3) FScale(k float64) *Vector3 {
	u.X *= k
	u.Y *= k
	u.Z *= k

	return &u
}

/*
NormSquared calculates the squared length of the calling vector.
*/
func (u Vector3) NormSquared() float64 {
	return u.X*u.X + u.Y*u.Y + u.Z*u.Z
}

/*
Norm calculates the length of the calling vector.
*/
func (u Vector3) Norm() float64 {
	return math.Sqrt(u.X*u.X + u.Y*u.Y + u.Z*u.Z)
}

/*
Normalize normalizes the calling vector, returning a vector with a length equal to 1.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FNormalize().
*/
func (u *Vector3) Normalize() {
	k := 1 / math.Sqrt(u.X*u.X+u.Y*u.Y+u.Z*u.Z)
	u.X *= k
	u.Y *= k
	u.Z *= k
}

/*
FNormalize normalizes the calling vector, returning a vector with a length equal to 1.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Normalize().
*/
func (u Vector3) FNormalize() *Vector3 {
	k := 1 / math.Sqrt(u.X*u.X+u.Y*u.Y+u.Z*u.Z)
	u.X *= k
	u.Y *= k
	u.Z *= k

	return &u
}

/*
RotateM rotates the calling vector over angle radians along a given axis using a matrix.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FRotateM(angle, axis).
*/
func (u *Vector3) RotateM(angle float64, axis *Vector3) {
	cos, sin := math.Cos(angle), math.Sin(angle)
	k := 1 - cos
	u.X, u.Y, u.Z =
		u.X*(axis.X*axis.X*k+cos)+u.Y*(axis.X*axis.Y*k-axis.Z*sin)+u.Z*(axis.X*axis.Z*k+axis.Y*sin),
		u.X*(axis.X*axis.Y*k+axis.Z*sin)+u.Y*(axis.Y*axis.Y*k+cos)+u.Z*(axis.Y*axis.Z*k-axis.X*sin),
		u.X*(axis.X*axis.Z*k-axis.Y*sin)+u.Y*(axis.Y*axis.Z*k+axis.X*sin)+u.Z*(axis.Z*axis.Z*k+cos)
}

/*
FRotateM rotates the calling vector over angle radians along a given axis using a matrix.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.RotateM(angle, axis).
*/
func (u Vector3) FRotateM(angle float64, axis *Vector3) *Vector3 {
	cos, sin := math.Cos(angle), math.Sin(angle)
	k := 1 - cos
	u.X, u.Y, u.Z =
		u.X*(axis.X*axis.X*k+cos)+u.Y*(axis.X*axis.Y*k-axis.Z*sin)+u.Z*(axis.X*axis.Z*k+axis.Y*sin),
		u.X*(axis.X*axis.Y*k+axis.Z*sin)+u.Y*(axis.Y*axis.Y*k+cos)+u.Z*(axis.Y*axis.Z*k-axis.X*sin),
		u.X*(axis.X*axis.Z*k-axis.Y*sin)+u.Y*(axis.Y*axis.Z*k+axis.X*sin)+u.Z*(axis.Z*axis.Z*k+cos)

	return &u
}

/*
Rotate rotates the calling vector over angle radians along a given axis using quaternions.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

Make sure the calling vector is normalized before calling this function!

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FRotate(angle, axis).
*/
func (u *Vector3) Rotate(angle float64, axis *Vector3) {
	cos, sin := math.Cos(angle), math.Sin(angle)
	k := (1 - cos) * (axis.X*u.X + axis.Y*u.Y + axis.Z*u.Z)
	u.X, u.Y, u.Z =
		k*axis.X+cos*u.X+sin*(axis.Y*u.Z-axis.Z*u.Y),
		k*axis.Y+cos*u.Y+sin*(axis.Z*u.X-axis.X*u.Z),
		k*axis.Z+cos*u.Z+sin*(axis.X*u.Y-axis.Y*u.X)
}

/*
FRotate rotates the calling vector over angle radians along a given axis using quaternions.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

Make sure the calling vector is normalized before calling this function!

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.Rotate(angle, axis).
*/
func (u Vector3) FRotate(angle float64, axis *Vector3) *Vector3 {
	cos, sin := math.Cos(angle), math.Sin(angle)
	k := (1 - cos) * (axis.X*u.X + axis.Y*u.Y + axis.Z*u.Z)
	u.X, u.Y, u.Z =
		k*axis.X+cos*u.X+sin*(axis.Y*u.Z-axis.Z*u.Y),
		k*axis.Y+cos*u.Y+sin*(axis.Z*u.X-axis.X*u.Z),
		k*axis.Z+cos*u.Z+sin*(axis.X*u.Y-axis.Y*u.X)

	return &u
}

/*
RotateMAndTranslate rotates the calling vector over angle radians along a given axis and translates it according to the given translation using a matrix.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FRotateMAndTranslate(angle, axis).
*/
func (u *Vector3) RotateMAndTranslate(angle float64, axis *Vector3, translation *Vector3) {
	cos, sin := math.Cos(angle), math.Sin(angle)
	k := 1 - cos
	u.X, u.Y, u.Z =
		u.X*(axis.X*axis.X*k+cos)+u.Y*(axis.X*axis.Y*k-axis.Z*sin)+u.Z*(axis.X*axis.Z*k+axis.Y*sin)+translation.X,
		u.X*(axis.X*axis.Y*k+axis.Z*sin)+u.Y*(axis.Y*axis.Y*k+cos)+u.Z*(axis.Y*axis.Z*k-axis.X*sin)+translation.Y,
		u.X*(axis.X*axis.Z*k-axis.Y*sin)+u.Y*(axis.Y*axis.Z*k+axis.X*sin)+u.Z*(axis.Z*axis.Z*k+cos)+translation.Z
}

/*
FRotateMAndTranslate rotates the calling vector over angle radians along a given axis and translates it according to the given translation using a matrix.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.RotateMAndTranslate(angle, axis).
*/
func (u Vector3) FRotateMAndTranslate(angle float64, axis *Vector3, translation *Vector3) *Vector3 {
	cos, sin := math.Cos(angle), math.Sin(angle)
	k := 1 - cos
	u.X, u.Y, u.Z =
		u.X*(axis.X*axis.X*k+cos)+u.Y*(axis.X*axis.Y*k-axis.Z*sin)+u.Z*(axis.X*axis.Z*k+axis.Y*sin)+translation.X,
		u.X*(axis.X*axis.Y*k+axis.Z*sin)+u.Y*(axis.Y*axis.Y*k+cos)+u.Z*(axis.Y*axis.Z*k-axis.X*sin)+translation.Y,
		u.X*(axis.X*axis.Z*k-axis.Y*sin)+u.Y*(axis.Y*axis.Z*k+axis.X*sin)+u.Z*(axis.Z*axis.Z*k+cos)+translation.Z

	return &u
}

/*
RotateAndTranslate rotates the calling vector over angle radians along a given axis and translates it according to the given translation using quaternions.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is modified and overwritten.
If a functional operation is needed, use u.FRotateMAndTranslate(angle, axis).
*/
func (u *Vector3) RotateAndTranslate(angle float64, axis *Vector3, translation *Vector3) {
	cos, sin := math.Cos(angle), math.Sin(angle)
	k := (1 - cos) * (axis.X*u.X + axis.Y*u.Y + axis.Z*u.Z)
	u.X, u.Y, u.Z =
		k*axis.X+cos*u.X+sin*(axis.Y*u.Z-axis.Z*u.Y)+translation.X,
		k*axis.Y+cos*u.Y+sin*(axis.Z*u.X-axis.X*u.Z)+translation.Y,
		k*axis.Z+cos*u.Z+sin*(axis.X*u.Y-axis.Y*u.X)+translation.Z
}

/*
FRotateAndTranslate rotates the calling vector over angle radians along a given axis and translates it according to the given translation using quaternions.
A positive angle rotates anti-clockwise, a negative angle rotates clockwise.

The calling vector itself is not modified, a copy is created instead.
If an in-place operation is needed, use u.RotateMAndTranslate(angle, axis).
*/
func (u Vector3) FRotateAndTranslate(angle float64, axis *Vector3, translation *Vector3) *Vector3 {
	cos, sin := math.Cos(angle), math.Sin(angle)
	k := (1 - cos) * (axis.X*u.X + axis.Y*u.Y + axis.Z*u.Z)
	u.X, u.Y, u.Z =
		k*axis.X+cos*u.X+sin*(axis.Y*u.Z-axis.Z*u.Y)+translation.X,
		k*axis.Y+cos*u.Y+sin*(axis.Z*u.X-axis.X*u.Z)+translation.Y,
		k*axis.Z+cos*u.Z+sin*(axis.X*u.Y-axis.Y*u.X)+translation.Z

	return &u
}
