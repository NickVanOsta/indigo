package mathengine

import (
	"fmt"
	"math"
	"testing"
)

// === EXAMPLES ===

func ExampleZeroVector3() {
	fmt.Println(ZeroVector3())
	//Output:
	// [0 0 0]
}

func ExampleUnitXVector3() {
	fmt.Println(UnitXVector3())
	//Output:
	// [1 0 0]
}

func ExampleUnitYVector3() {
	fmt.Println(UnitYVector3())
	//Output:
	// [0 1 0]
}

func ExampleUnitZVector3() {
	fmt.Println(UnitZVector3())
	//Output:
	// [0 0 1]
}

func ExampleVector3_Add() {
	u := Vector3{2, 3, 4}
	v := Vector3{4, 9, 16}
	w := Vector3{8, 27, 64}

	u.Add(&v, &w)
	fmt.Println(u)
	// Output:
	// [14 39 84]
}

func ExampleVector3_FAdd() {
	u := Vector3{2, 3, 4}
	v := Vector3{4, 9, 16}
	w := Vector3{8, 27, 64}

	fmt.Println(u.FAdd(&v, &w))
	// Output:
	// [14 39 84]
}

func ExampleVector3_Subtract() {
	u := Vector3{2, 3, 4}
	v := Vector3{4, 9, 16}
	w := Vector3{8, 27, 64}

	u.Subtract(&v, &w)
	fmt.Println(u)
	// Output:
	// [-10 -33 -76]
}

func ExampleVector3_FSubtract() {
	u := Vector3{2, 3, 4}
	v := Vector3{4, 9, 16}
	w := Vector3{8, 27, 64}

	fmt.Println(u.FSubtract(&v, &w))
	// Output:
	// [-10 -33 -76]
}

func ExampleVector3_Dot() {
	u := Vector3{2, 3, 4}
	v := Vector3{4, 9, 16}

	fmt.Println(u.Dot(&v))
	// Output:
	// 99
}

func ExampleVector3_Cross() {
	u := Vector3{2, 3, 4}
	v := Vector3{4, 9, 16}
	w := Vector3{8, 27, 64}

	u.Cross(&v, &w)
	fmt.Println(u)
	// Output:
	// [-1186 -720 452]
}

func ExampleVector3_FCross() {
	u := Vector3{2, 3, 4}
	v := Vector3{4, 9, 16}
	w := Vector3{8, 27, 64}

	fmt.Println(u.FCross(&v, &w))
	// Output:
	// [-1186 -720 452]
}

func ExampleVector3_Scale() {
	u := Vector3{2, 3, 4}
	k := 42.

	u.Scale(k)
	fmt.Println(u)
	// Output:
	// [84 126 168]
}

func ExampleVector3_FScale() {
	u := Vector3{2, 3, 4}
	k := 42.

	fmt.Println(u.FScale(k))
	// Output:
	// [84 126 168]
}

func ExampleVector3_NormSquared() {
	u := Vector3{9, 12, 20}

	fmt.Println(u.NormSquared())
	// Output:
	// 625
}

func ExampleVector3_Norm() {
	u := Vector3{9, 12, 20}

	fmt.Println(u.Norm())
	// Output:
	// 25
}

func ExampleVector3_Normalize() {
	u := Vector3{9, 12, 20}

	u.Normalize()
	fmt.Println(u)
	// Output:
	// [0.36 0.48 0.8]
}

func ExampleVector3_FNormalize() {
	u := Vector3{9, 12, 20}

	fmt.Println(u.FNormalize())
	// Output:
	// [0.36 0.48 0.8]
}

func ExampleVector3_RotateM() {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}

	u.RotateM(angle, &axis)
	fmt.Println(u)
	//Output:
	// [1.8369701987210272e-16 0 -3]
}

func ExampleVector3_FRotateM() {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}

	fmt.Println(u.FRotateM(angle, &axis))
	//Output:
	// [1.8369701987210272e-16 0 -3]
}

func ExampleVector3_Rotate() {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}

	u.Rotate(angle, &axis)
	fmt.Println(u)
	//Output:
	// [1.8369701987210272e-16 0 -3]
}

func ExampleVector3_FRotate() {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}

	fmt.Println(u.FRotate(angle, &axis))
	//Output:
	// [1.8369701987210272e-16 0 -3]
}

func ExampleVector3_RotateMAndTranslate() {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}
	t := Vector3{1, 2, 3}

	u.RotateMAndTranslate(angle, &axis, &t)
	fmt.Println(u)
	//Output:
	// [1.0000000000000002 2 0]
}

func ExampleVector3_FRotateMAndTranslate() {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}
	t := Vector3{1, 2, 3}

	fmt.Println(u.FRotateMAndTranslate(angle, &axis, &t))
	//Output:
	// [1.0000000000000002 2 0]
}

func ExampleVector3_RotateAndTranslate() {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}
	t := Vector3{1, 2, 3}

	u.RotateAndTranslate(angle, &axis, &t)
	fmt.Println(u)
	//Output:
	// [1.0000000000000002 2 0]
}

func ExampleVector3_FRotateAndTranslate() {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}
	t := Vector3{1, 2, 3}

	fmt.Println(u.FRotateAndTranslate(angle, &axis, &t))
	//Output:
	// [1.0000000000000002 2 0]
}

// === TESTS ===

func TestZeroVector3(t *testing.T) {
	w := Vector3{0, 0, 0}
	if o := *ZeroVector3(); o != w {
		t.Errorf("Expected %v, but got %v.\n", w, o)
	}
}

func TestUnitXVector3(t *testing.T) {
	w := Vector3{1, 0, 0}
	if ex := *UnitXVector3(); ex != w {
		t.Errorf("Expected %v, but got %v.\n", w, ex)
	}
}

func TestUnitYVector3(t *testing.T) {
	w := Vector3{0, 1, 0}
	if ey := *UnitYVector3(); ey != w {
		t.Errorf("Expected %v, but got %v.\n", w, ey)
	}
}

func TestUnitZVector3(t *testing.T) {
	w := Vector3{0, 0, 1}
	if ez := *UnitZVector3(); ez != w {
		t.Errorf("Expected %v, but got %v.\n", w, ez)
	}
}

func TestVector3_Add(t *testing.T) {
	ex := Vector3{1, 0, 0}
	ey := Vector3{0, 1, 0}

	t.Run("Adds 2 vectors correctly", func(t *testing.T) {
		u := Vector3{3, 3, 3}
		w := Vector3{3, 4, 3}
		if u.Add(&ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})

	t.Run("Adds 3 vectors correctly", func(t *testing.T) {
		u := Vector3{3, 3, 3}
		w := Vector3{4, 4, 3}
		if u.Add(&ex, &ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})
}

func TestVector3_FAdd(t *testing.T) {
	ex := Vector3{1, 0, 0}
	ey := Vector3{0, 1, 0}
	u := Vector3{3, 3, 3}

	t.Run("Adds 2 vectors correctly", func(t *testing.T) {
		w := Vector3{3, 4, 3}
		if v := *u.FAdd(&ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Adds 3 vectors correctly", func(t *testing.T) {
		w := Vector3{4, 4, 3}
		if v := *u.FAdd(&ex, &ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Creates a copy when no vectors are given", func(t *testing.T) {
		v := *u.FAdd()
		if v != u {
			t.Errorf("Expected %v, but got %v.\n", u, v)
		}
		if &v == &u {
			t.Errorf("Expected not %v, but got %v.\n", &u, &v)
		}
	})
}

func TestVector3_Subtract(t *testing.T) {
	ex := Vector3{1, 0, 0}
	ey := Vector3{0, 1, 0}

	t.Run("Subtracts 2 vectors correctly", func(t *testing.T) {
		u := Vector3{3, 3, 3}
		w := Vector3{3, 2, 3}
		if u.Subtract(&ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})

	t.Run("Subtracts 3 vectors correctly", func(t *testing.T) {
		u := Vector3{3, 3, 3}
		w := Vector3{2, 2, 3}
		if u.Subtract(&ex, &ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})
}

func TestVector3_FSubtract(t *testing.T) {
	ex := Vector3{1, 0, 0}
	ey := Vector3{0, 1, 0}
	u := Vector3{3, 3, 3}

	t.Run("Subtracts 2 vectors correctly", func(t *testing.T) {
		w := Vector3{3, 2, 3}
		if v := *u.FSubtract(&ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Subtracts 3 vectors correctly", func(t *testing.T) {
		w := Vector3{2, 2, 3}
		if v := *u.FSubtract(&ex, &ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Creates a copy when no vectors are given", func(t *testing.T) {
		v := *u.FSubtract()
		if v != u {
			t.Errorf("Expected %v, but got %v.\n", u, v)
		}
		if &v == &u {
			t.Errorf("Expected not %v, but got %v.\n", &u, &v)
		}
	})
}

func TestVector3_Dot(t *testing.T) {
	u := Vector3{2, -3, 4}
	v := Vector3{5, 2, 1}

	t.Run("Calculates the correct dot product of the given vectors", func(t *testing.T) {
		w := 8.
		if x := u.Dot(&v); x != w {
			t.Errorf("Expected %v, but got %v.\n", w, x)
		}
	})
}

func TestVector3_Cross(t *testing.T) {
	ex := Vector3{1, 0, 0}
	ey := Vector3{0, 1, 0}

	t.Run("Subtracts 2 vectors correctly", func(t *testing.T) {
		u := Vector3{3, 3, 3}
		w := Vector3{-3, 0, 3}
		if u.Cross(&ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})

	t.Run("Subtracts 3 vectors correctly", func(t *testing.T) {
		u := Vector3{3, 3, 3}
		w := Vector3{3, 0, 0}
		if u.Cross(&ex, &ey); u != w {
			t.Errorf("Expected %v, but got %v.\n", w, u)
		}
	})
}

func TestVector3_FCross(t *testing.T) {
	ex := Vector3{1, 0, 0}
	ey := Vector3{0, 1, 0}
	u := Vector3{3, 3, 3}

	t.Run("Subtracts 2 vectors correctly", func(t *testing.T) {
		w := Vector3{-3, 0, 3}
		if v := *u.FCross(&ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Subtracts 3 vectors correctly", func(t *testing.T) {
		w := Vector3{3, 0, 0}
		if v := *u.FCross(&ex, &ey); v != w {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})

	t.Run("Creates a copy when no vectors are given", func(t *testing.T) {
		v := *u.FCross()
		if v != u {
			t.Errorf("Expected %v, but got %v.\n", u, v)
		}
		if &v == &u {
			t.Errorf("Expected not %v, but got %v.\n", &u, &v)
		}
	})
}

func TestVector3_FScale(t *testing.T) {
	u := Vector3{1, 1, 1}
	k := 5.

	t.Run("Scales the vector with a factor >1 correctly", func(t *testing.T) {
		w := Vector3{5, 5, 5}
		if x := *u.FScale(k); x != w {
			t.Errorf("Expected %v, but got %v.\n", w, x)
		}
	})

	t.Run("Scales the vector with a factor <1 correctly", func(t *testing.T) {
		w := Vector3{.2, .2, .2}
		if x := *u.FScale(1 / k); x != w {
			t.Errorf("Expected %v, but got %v.\n", w, x)
		}
	})
}

func TestVector3_NormSquared(t *testing.T) {
	u := Vector3{9, 12, 20}

	t.Run("Calculates the squared length of the vector correctly", func(t *testing.T) {
		w := 625.
		if x := u.NormSquared(); x != w {
			t.Errorf("Expected %v, but got %v.\n", w, x)
		}
	})
}

func TestVector3_Norm(t *testing.T) {
	u := Vector3{9, 12, 20}

	t.Run("Calculates the length of the vector correctly", func(t *testing.T) {
		w := 25.
		if x := u.Norm(); x != w {
			t.Errorf("Expected %v, but got %v.\n", w, x)
		}
	})
}

func TestVector3_FNormalize(t *testing.T) {
	u := Vector3{9, 12, 20}

	t.Run("Normalizes the vector correctly", func(t *testing.T) {
		w := Vector3{.36, .48, .8}
		if v := *u.FNormalize(); v.X-w.X > 0.0001 || v.Y-w.Y > 0.0001 || v.Z-w.Z > 0.0001 {
			t.Errorf("Expected %v, but got %v.\n", w, v)
		}
	})
}

func TestVector3_RotateM(t *testing.T) {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}

	w := Vector3{0, 0, -3}
	if u.RotateM(angle, &axis); u.X-w.X > 0.0001 || u.Y-w.Y > 0.0001 || u.Z-w.Z > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, u)
	}
}

func TestVector3_FRotateM(t *testing.T) {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}

	w := Vector3{0, 0, -3}
	if v := *u.FRotateM(angle, &axis); v.X-w.X > 0.0001 || v.Y-w.Y > 0.0001 || v.Z-w.Z > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, v)
	}
}

func TestVector3_Rotate(t *testing.T) {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}

	w := Vector3{0, 0, -3}
	if u.Rotate(angle, &axis); u.X-w.X > 0.0001 || u.Y-w.Y > 0.0001 || u.Z-w.Z > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, u)
	}
}

func TestVector3_FRotate(t *testing.T) {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}

	w := Vector3{0, 0, -3}
	if v := *u.FRotate(angle, &axis); v.X-w.X > 0.0001 || v.Y-w.Y > 0.0001 || v.Z-w.Z > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, v)
	}
}

func TestVector3_RotateMAndTranslate(t *testing.T) {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}
	translation := Vector3{1, 2, 3}

	w := Vector3{1, 2, 0}
	if u.RotateMAndTranslate(angle, &axis, &translation); u.X-w.X > 0.0001 || u.Y-w.Y > 0.0001 || u.Z-w.Z > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, u)
	}
}

func TestVector3_FRotateMAndTranslate(t *testing.T) {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}
	translation := Vector3{1, 2, 3}

	w := Vector3{1, 2, 0}
	if v := *u.FRotateMAndTranslate(angle, &axis, &translation); v.X-w.X > 0.0001 || v.Y-w.Y > 0.0001 || v.Z-w.Z > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, v)
	}
}

func TestVector3_RotateAndTranslate(t *testing.T) {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}
	translation := Vector3{1, 2, 3}

	w := Vector3{1, 2, 0}
	if u.RotateAndTranslate(angle, &axis, &translation); u.X-w.X > 0.0001 || u.Y-w.Y > 0.0001 || u.Z-w.Z > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, u)
	}
}

func TestVector3_FRotateAndTranslate(t *testing.T) {
	u := Vector3{3, 0, 0}
	// Rotate 90 degrees along the Y-axis
	angle, axis := math.Pi*.5, Vector3{0, 1, 0}
	translation := Vector3{1, 2, 3}

	w := Vector3{1, 2, 0}
	if v := *u.FRotateAndTranslate(angle, &axis, &translation); v.X-w.X > 0.0001 || v.Y-w.Y > 0.0001 || v.Z-w.Z > 0.0001 {
		t.Errorf("Expected %v, but got %v.\n", w, v)
	}
}

// === BENCHMARKS ===

func BenchmarkZeroVector3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ZeroVector3()
	}
}

func BenchmarkUnitXVector3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		UnitXVector3()
	}
}

func BenchmarkUnitYVector3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		UnitYVector3()
	}
}

func BenchmarkUnitZVector3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		UnitZVector3()
	}
}

func BenchmarkVector3_Add(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.Run("Add 2 vectors", func(b *testing.B) {
		v := Vector3{6, 8, 10}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Add(&v)
		}
	})

	b.Run("Add 8 vectors", func(b *testing.B) {
		v := []*Vector3{{6, 8, 10}, {9, 12, 15}, {12, 16, 20}, {15, 20, 25}, {18, 24, 30}, {21, 28, 35}, {24, 32, 40}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Add(v...)
		}
	})

	b.Run("Add no vectors", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Add()
		}
	})
}

func BenchmarkVector3_FAdd(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.Run("Add 2 vectors", func(b *testing.B) {
		v := Vector3{6, 8, 10}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FAdd(&v)
		}
	})

	b.Run("Add 8 vectors", func(b *testing.B) {
		v := []*Vector3{{6, 8, 10}, {9, 12, 15}, {12, 16, 20}, {15, 20, 25}, {18, 24, 30}, {21, 28, 35}, {24, 32, 40}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FAdd(v...)
		}
	})

	b.Run("Copy a vector", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FAdd()
		}
	})
}

func BenchmarkVector3_Subtract(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.Run("Subtract 2 vectors", func(b *testing.B) {
		v := Vector3{6, 8, 10}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Subtract(&v)
		}
	})

	b.Run("Subtract 8 vectors", func(b *testing.B) {
		v := []*Vector3{{6, 8, 10}, {9, 12, 15}, {12, 16, 20}, {15, 20, 25}, {18, 24, 30}, {21, 28, 35}, {24, 32, 40}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Subtract(v...)
		}
	})

	b.Run("Add no vectors", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Subtract()
		}
	})
}

func BenchmarkVector3_FSubtract(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.Run("Subtract 2 vectors", func(b *testing.B) {
		v := Vector3{6, 8, 10}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FSubtract(&v)
		}
	})

	b.Run("Subtract 8 vectors", func(b *testing.B) {
		v := []*Vector3{{6, 8, 10}, {9, 12, 15}, {12, 16, 20}, {15, 20, 25}, {18, 24, 30}, {21, 28, 35}, {24, 32, 40}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FSubtract(v...)
		}
	})

	b.Run("Copy a vector", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FSubtract()
		}
	})
}

func BenchmarkVector3_Dot(b *testing.B) {
	u := &Vector3{3, 4, 5}
	v := &Vector3{6, 8, 10}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Dot(v)
	}
}

func BenchmarkVector3_Cross(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.Run("Calculate the cross product of 2 vectors", func(b *testing.B) {
		v := Vector3{6, 8, 10}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Cross(&v)
		}
	})

	b.Run("Calculate the cross product of 8 vectors", func(b *testing.B) {
		v := []*Vector3{{6, 8, 10}, {9, 12, 15}, {12, 16, 20}, {15, 20, 25}, {18, 24, 30}, {21, 28, 35}, {24, 32, 40}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Cross(v...)
		}
	})

	b.Run("Calculate the cross product of no vectors", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.Cross()
		}
	})
}

func BenchmarkVector3_FCross(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.Run("Calculate the cross product of 2 vectors", func(b *testing.B) {
		v := Vector3{6, 8, 10}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FCross(&v)
		}
	})

	b.Run("Calculate the cross product of 8 vectors", func(b *testing.B) {
		v := []*Vector3{{6, 8, 10}, {9, 12, 15}, {12, 16, 20}, {15, 20, 25}, {18, 24, 30}, {21, 28, 35}, {24, 32, 40}}

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FCross(v...)
		}
	})

	b.Run("Calculate the cross product of no vectors", func(b *testing.B) {
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			u.FCross()
		}
	})
}

func BenchmarkVector3_Scale(b *testing.B) {
	u := &Vector3{3, 4, 5}
	k := 3.14159265359

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Scale(k)
	}
}

func BenchmarkVector3_FScale(b *testing.B) {
	u := &Vector3{3, 4, 5}
	k := 3.14159265359

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FScale(k)
	}
}

func BenchmarkVector3_NormSquared(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.NormSquared()
	}
}

func BenchmarkVector3_Norm(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Norm()
	}
}

func BenchmarkVector3_Normalize(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Normalize()
	}
}

func BenchmarkVector3_FNormalize(b *testing.B) {
	u := &Vector3{3, 4, 5}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FNormalize()
	}
}

func BenchmarkVector3_RotateM(b *testing.B) {
	u := &Vector3{3, 4, 5}
	angle, axis := 1., &Vector3{1, 2, 3}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.RotateM(angle, axis)
	}
}

func BenchmarkVector3_FRotateM(b *testing.B) {
	u := &Vector3{3, 4, 5}
	angle, axis := 1., &Vector3{1, 2, 3}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FRotateM(angle, axis)
	}
}

func BenchmarkVector3_Rotate(b *testing.B) {
	u := &Vector3{3, 4, 5}
	angle, axis := 1., &Vector3{1, 2, 3}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.Rotate(angle, axis)
	}
}

func BenchmarkVector3_FRotate(b *testing.B) {
	u := &Vector3{3, 4, 5}
	angle, axis := 1., &Vector3{1, 2, 3}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FRotate(angle, axis)
	}
}

func BenchmarkVector3_RotateMAndTranslate(b *testing.B) {
	u := &Vector3{3, 4, 5}
	angle, axis := 1., &Vector3{1, 2, 3}
	translation := &Vector3{3, 2, 1}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.RotateMAndTranslate(angle, axis, translation)
	}
}

func BenchmarkVector3_FRotateMAndTranslate(b *testing.B) {
	u := &Vector3{3, 4, 5}
	angle, axis := 1., &Vector3{1, 2, 3}
	translation := &Vector3{3, 2, 1}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FRotateMAndTranslate(angle, axis, translation)
	}
}

func BenchmarkVector3_RotateAndTranslate(b *testing.B) {
	u := &Vector3{3, 4, 5}
	angle, axis := 1., &Vector3{1, 2, 3}
	translation := &Vector3{3, 2, 1}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.RotateAndTranslate(angle, axis, translation)
	}
}

func BenchmarkVector3_FRotateAndTranslate(b *testing.B) {
	u := &Vector3{3, 4, 5}
	angle, axis := 1., &Vector3{1, 2, 3}
	translation := &Vector3{3, 2, 1}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		u.FRotateAndTranslate(angle, axis, translation)
	}
}
