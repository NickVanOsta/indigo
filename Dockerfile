# Based off of this Dockerfile:
# https://gitlab.com/pantomath-io/demo-tools/-/blob/master/Dockerfile

# Base image:
FROM golang:latest
LABEL maintainer="van_osta_nick@hotmail.com"
LABEL version="0.1.0"

# Install golint
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH
RUN go get -u golang.org/x/lint/golint

# Add apt key for LLVM repository
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# Add LLVM apt repository
RUN echo "# LLVM\n \
  deb http://apt.llvm.org/buster/ llvm-toolchain-buster main\n \
  deb-src http://apt.llvm.org/buster/ llvm-toolchain-buster main\n \
  # 9\n \
  deb http://apt.llvm.org/buster/ llvm-toolchain-buster-9 main\n \
  deb-src http://apt.llvm.org/buster/ llvm-toolchain-buster-9 main\n \
  # 10\n \
  deb http://apt.llvm.org/buster/ llvm-toolchain-buster-10 main\n \
  deb-src http://apt.llvm.org/buster/ llvm-toolchain-buster-10 main" | tee -a /etc/apt/sources.list

# Install clang from LLVM repository
RUN apt-get update && apt-get install -y --no-install-recommends \
  clang-10 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set Clang as default CC
ENV set_clang /etc/profile.d/set-clang-cc.sh
RUN echo "export CC=clang-10" | tee -a ${set_clang} && chmod a+x ${set_clang}
